package universite.angers.master.info.orm.bd.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation qui permet d'identifier un attribut colonne pour la BD dans le moteur de réflection
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@Retention(RUNTIME) //Paramétrer pour le moteur de réflection
@Target(FIELD) //Porter uniquement sur les attributs
public @interface Column {
	String name() default "";
	String[] constraints() default {};
}
