package universite.angers.master.info.orm.bd.table;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import universite.angers.master.info.orm.bd.Executable;
import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.bd.annotation.Table;
import universite.angers.master.info.orm.bd.column.ColumnForeignKeyORM;
import universite.angers.master.info.orm.bd.column.ColumnORM;
import universite.angers.master.info.orm.bd.column.ColumnPrimaryKeyORM;
import universite.angers.master.info.orm.util.UtilORM;

/**
 * Classe qui permet d'associer une table avec une classe d'un objet
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class TableORM implements Executable {

	private static final Logger LOG = Logger.getLogger(TableORM.class);
	
	/**
	 * La table est-elle auto-incrément ?
	 */
	protected boolean autoIncrementValue;
	
	/**
	 * La classe de l'objet
	 */
	protected Class<?> object;
	
	/**
	 * Savoir si la classe est instanciable
	 * Par défaut abstraite = non instanciable
	 */
	protected boolean instanciableObject;
	
	/**
	 * Les instances d'objets présents dans la table
	 * Cette liste permet d'éviter d'enregistrer en doublon les objets dans la BD
	 */
	protected List<Object> instances;
	
	/**
	 * La super classe représenté par une table
	 */
	protected TableORM tableParent;
	
	/**
	 * La classe hérités représentés par des tables
	 */
	protected Map<String, TableORM> tablesChildrens;
	
	/**
	 * Le nom de la table
	 */
	protected String name;
	
	/**
	 * Les colonnes primitives
	 */
	protected Map<String, ColumnORM> columns;
	
	/**
	 * Les colonnes clés primaires
	 */
	protected Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys;
	
	/**
	 * Les colonnes étrangères
	 */
	protected Map<String, ColumnForeignKeyORM> columnsForeignKeys;

	public TableORM(String name, Map<String, ColumnORM> columns, Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys,
			Map<String, ColumnForeignKeyORM> columnsForeignKeys) {
		this.autoIncrementValue = this.initAutoIncrementValue();
		LOG.debug("Auto increment : " + this.autoIncrementValue);
		
		this.instances = new ArrayList<>();
		LOG.debug("Instances : " + this.instances);
		
		this.instanciableObject = true;
		LOG.debug("Instanciable Object : " + this.instanciableObject);
		
		this.name = name;
		LOG.debug("Name : " + this.name);
		
		this.tableParent = null;
		LOG.debug("Table parent : " + this.tableParent);
		
		this.tablesChildrens = new HashMap<>();
		LOG.debug("Tables childrens : " + this.tablesChildrens);
		
		this.columns = columns;
		LOG.debug("Columns : " + this.columns);
		
		this.columnsPrimaryKeys = columnsPrimaryKeys;
		LOG.debug("PrimaryKeys : " + this.columnsPrimaryKeys);
		
		this.columnsForeignKeys = columnsForeignKeys;
		LOG.debug("ForeignKeys : " + this.columnsForeignKeys);
	}
	
	public TableORM(Class<?> object) {
		this.object = object;
		LOG.debug("Object : " + this.object);
		
		this.autoIncrementValue = this.initAutoIncrementValue();
		LOG.debug("Auto increment : " + this.autoIncrementValue);
		
		this.instances = new ArrayList<>();
		LOG.debug("Instances : " + this.instances);
		
		this.instanciableObject = this.initInstanciableObject();
		LOG.debug("Instanciable Object : " + this.instanciableObject);
		
		this.name = this.initName();
		LOG.debug("Name : " + this.name);
		
		this.tableParent = null;
		LOG.debug("Table parent : " + this.tableParent);
		
		this.tablesChildrens = new HashMap<>();
		LOG.debug("Tables childrens : " + this.tablesChildrens);
		
		this.columns = this.initColumns();
		LOG.debug("Columns : " + this.columns);
		
		this.columnsPrimaryKeys = this.initColumnsPrimaryKeys();
		LOG.debug("PrimaryKeys : " + this.columnsPrimaryKeys);
		
		this.columnsForeignKeys = this.initColumnsForeignKeys();
		LOG.debug("ForeignKeys : " + this.columnsForeignKeys);
	}
	
	public abstract ColumnORM newColumnORM(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue);
	public abstract ColumnORM newColumnORM(Field field);
	public abstract ColumnPrimaryKeyORM newColumnPrimaryKeyORM(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue);
	public abstract ColumnPrimaryKeyORM newColumnPrimaryKeyORM(Field field);
	public abstract ColumnForeignKeyORM newColumnForeignKeyORM(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue, 
			TableORM tableSource, TableORM tableTarget, TableORM tableAssociation);
	public abstract ColumnForeignKeyORM newColumnForeignKeyORM(Field field, TableORM tableSource, TableORM tableTarget, TableORM tableAssociation);
	
	//Les actions sur les tables
	public abstract String getConstraintOnCreate();
	public abstract String getConstraintOnRead();
	public abstract String getConstraintOnUpdate();
	public abstract String getConstraintOnDelete();
	
	//Opérations CRUD
	
	public abstract String getCreateCommandSQL(int size);
	public abstract String getReadCommandSQL();
	public abstract String getReadCommandSQL(Map<? extends ColumnORM, Object> columnsWhere);
	public abstract String getReadCommandSQL(Map<String, ? extends ColumnORM> columnsSelect, 
			Map<? extends ColumnORM, Object> columnsWhere);
	public abstract String getUpdateCommandSQL(Map<String, ? extends ColumnORM> columnsWhere);
	public abstract String getDeleteCommandSQL(Map<String, ? extends ColumnORM> columnsWhere);
	
	private boolean initAutoIncrementValue() {
		if(this.object == null) return false;
		
		if(this.object.isAnnotationPresent(Table.class)) {
			Table table = this.object.getDeclaredAnnotation(Table.class);
			if(table == null) return false;
			
			return table.isAutoIncrement();
		} else return false;
	}
	
	private boolean initInstanciableObject() {
		//Par défaut une classe abstraite n'est pas instanciable
		if(Modifier.isAbstract(this.object.getModifiers())) return false;
		
		if(!this.object.isAnnotationPresent(Table.class)) return false;
		Table table = this.object.getDeclaredAnnotation(Table.class);
		
		return table.isInstanciable();
	}
	
	private String initName() {
		if(!this.object.isAnnotationPresent(Table.class)) return "";
		Table table = this.object.getDeclaredAnnotation(Table.class);
		
		//Si la table n'a pas de nom dans ce cas on retourne le nom de la classe de l'objet
		if(!UtilORM.isNullOrEmpty(table.name()))
			return table.name();
		else
			return this.object.getSimpleName();
	}
	
	private Map<String, ColumnORM> initColumns() {
		Map<String, ColumnORM> columns = new HashMap<>();
		
		//On ajoute d'abord les colonnes déclarées
		for(Field field : UtilORM.getAllDeclaredField(this.object, Column.class)) {
			ColumnORM column = this.newColumnORM(field);
			if(!columns.containsKey(column.getName()))
				columns.put(column.getName(), column);
		}
		
		return columns;
	}
	
	private Map<String, ColumnPrimaryKeyORM> initColumnsPrimaryKeys() {
		Map<String, ColumnPrimaryKeyORM> columns = new HashMap<>();
		
		for(Field field : UtilORM.getAllDeclaredField(this.object, PrimaryKey.class)) {
			ColumnPrimaryKeyORM column = this.newColumnPrimaryKeyORM(field);
			if(!columns.containsKey(column.getName()))
				columns.put(column.getName(), column);
		}
		
		return columns;
	}

	public Map<String, ColumnForeignKeyORM> initColumnsForeignKeys() {
		Map<String, ColumnForeignKeyORM> columns = new HashMap<>();
		
		for(Field field : UtilORM.getAllDeclaredField(this.object, ForeignKey.class)) {
			ColumnForeignKeyORM column = this.newColumnForeignKeyORM(field, this, null, null);
			if(!columns.containsKey(column.getName()))
				columns.put(column.getName(), column);
		}
		
		return columns;
	}

	/**
	 * Nettoye toutes les instances d'objets
	 */
	public void clear() {
		this.instances.clear();
		
		if(this.tableParent != null)
			this.tableParent.getInstances().clear();
		
		for(TableORM table : this.tablesChildrens.values()) {
			table.clear();
		}
		
		for(ColumnForeignKeyORM column : this.columnsForeignKeys.values()) {
			if(column.getTableSource() != null)
				column.getTableSource().getInstances().clear();
			
			if(column.getTableTarget() != null)
				column.getTableTarget().getInstances().clear();
			
			if(column.getTableAssociation() != null)
				column.getTableAssociation().getInstances().clear();
		}
	}

	/**
	 * Création de l'objet associé à la table par réflection
	 * Attention l'objet doit fournir un constructeur sans argument et public !
	 * @return
	 */
	public Object getNewObject() {
		try {
			return this.object.newInstance();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * @return the object
	 */
	public Class<?> getObject() {
		return object;
	}

	/**
	 * @param object the object to set
	 */
	public void setObject(Class<?> object) {
		this.object = object;
	}

	/**
	 * @return the autoIncrementValue
	 */
	public boolean isAutoIncrementValue() {
		return autoIncrementValue;
	}

	/**
	 * @param autoIncrementValue the autoIncrementValue to set
	 */
	public void setAutoIncrementValue(boolean autoIncrementValue) {
		this.autoIncrementValue = autoIncrementValue;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the columns
	 */
	public Map<String, ColumnORM> getColumns() {
		return columns;
	}

	/**
	 * @param columns the columns to set
	 */
	public void setColumns(Map<String, ColumnORM> columns) {
		this.columns = columns;
	}

	/**
	 * @return the columnsPrimaryKeys
	 */
	public Map<String, ColumnPrimaryKeyORM> getColumnsPrimaryKeys() {
		return columnsPrimaryKeys;
	}

	/**
	 * @param columnsPrimaryKeys the columnsPrimaryKeys to set
	 */
	public void setColumnsPrimaryKeys(Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys) {
		this.columnsPrimaryKeys = columnsPrimaryKeys;
	}

	/**
	 * @return the columnsForeignKeys
	 */
	public Map<String, ColumnForeignKeyORM> getColumnsForeignKeys() {
		return columnsForeignKeys;
	}

	/**
	 * @param columnsForeignKeys the columnsForeignKeys to set
	 */
	public void setColumnsForeignKeys(Map<String, ColumnForeignKeyORM> columnsForeignKeys) {
		this.columnsForeignKeys = columnsForeignKeys;
	}

	/**
	 * @return the tableParent
	 */
	public TableORM getTableParent() {
		return tableParent;
	}

	/**
	 * @param tableParent the tableParent to set
	 */
	public void setTableParent(TableORM tableParent) {
		this.tableParent = tableParent;
	}

	/**
	 * @return the tablesChildrens
	 */
	public Map<String, TableORM> getTablesChildrens() {
		return tablesChildrens;
	}

	/**
	 * @param tablesChildrens the tablesChildrens to set
	 */
	public void setTablesChildrens(Map<String, TableORM> tablesChildrens) {
		this.tablesChildrens = tablesChildrens;
	}
	
	/**
	 * @return the instances
	 */
	public List<Object> getInstances() {
		return instances;
	}

	/**
	 * @param instances the instances to set
	 */
	public void setInstances(List<Object> instances) {
		this.instances = instances;
	}

	/**
	 * @return the instanciableObject
	 */
	public boolean isInstanciableObject() {
		return instanciableObject;
	}

	/**
	 * @param instanciableObject the instanciableObject to set
	 */
	public void setInstanciableObject(boolean instanciableObject) {
		this.instanciableObject = instanciableObject;
	}
}
