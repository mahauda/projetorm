package universite.angers.master.info.orm.bd.jdbc.sqlite;

import universite.angers.master.info.orm.bd.FactoryORM;

/**
 * Factory qui permet de construire une base de données SQLite
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class FactorySQLite extends FactoryORM<SessionSQLite, DataBaseSQLite> {

	private static FactorySQLite instance;
	
	private FactorySQLite() {
		
	}
	
	public static synchronized FactorySQLite getInstance() {
		if(instance == null) {
			instance = new FactorySQLite();
		}
		
		return instance;
	}
	
	@Override
	public void build() {
		this.dataBase = new DataBaseSQLite(this.session, this.objects);
	}
}
