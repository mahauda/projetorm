package universite.angers.master.info.orm.bd.jdbc.sqlite;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import universite.angers.master.info.orm.bd.column.ColumnForeignKeyORM;
import universite.angers.master.info.orm.bd.column.ColumnORM;
import universite.angers.master.info.orm.bd.column.ColumnPrimaryKeyORM;
import universite.angers.master.info.orm.bd.table.TableORM;

/**
 * Classe table SQLite
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TableSQLite extends TableORM {

	private static final Logger LOG = Logger.getLogger(TableSQLite.class);
	
	public TableSQLite(String name, Map<String, ColumnORM> columns, Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys,
			Map<String, ColumnForeignKeyORM> columnsForeignKeys) {
		super(name, columns, columnsPrimaryKeys, columnsForeignKeys);
	}
	
	public TableSQLite(Class<?> object) {
		super(object);
	}

	@Override
	public ColumnORM newColumnORM(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue) {
		return new ColumnSQLite(field, clazz, name, constraints, defaultValue);
	}

	@Override
	public ColumnORM newColumnORM(Field field) {
		return new ColumnSQLite(field);
	}
	
	@Override
	public ColumnPrimaryKeyORM newColumnPrimaryKeyORM(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue) {
		return new ColumnPrimaryKeySQLite(field, clazz, name, constraints, defaultValue);
	}

	@Override
	public ColumnPrimaryKeyORM newColumnPrimaryKeyORM(Field field) {
		return new ColumnPrimaryKeySQLite(field);
	}

	@Override
	public ColumnForeignKeyORM newColumnForeignKeyORM(Field field, Class<?> clazz, String name, String[] constraints,
			Object defaultValue, TableORM tableSource, TableORM tableTarget, TableORM tableAssociation) {
		return new ColumnForeignKeySQLite(field, clazz, name, constraints, defaultValue, tableSource, tableTarget, tableAssociation);
	}

	@Override
	public ColumnForeignKeyORM newColumnForeignKeyORM(Field field, TableORM tableSource, TableORM tableTarget,
			TableORM tableAssociation) {
		return new ColumnForeignKeySQLite(field, tableSource, tableTarget, tableAssociation);
	}
	
	@Override
	public String executeCommandQueryCreateSQL() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("CREATE TABLE IF NOT EXISTS ");
		sb.append(this.name);
		sb.append("(\n");
		
		//On affiche d'abord les colonnes
		
		for(ColumnORM column : this.columns.values()) {
			LOG.debug("Colonnes normales : " + column.executeCommandQueryCreateSQL());
			sb.append(column.executeCommandQueryCreateSQL());
			sb.append(",\n");
		}
		
		//Puis les contraintes clés primaires
		
		if(!this.columnsPrimaryKeys.isEmpty()) {
			
			for(ColumnPrimaryKeyORM column : this.columnsPrimaryKeys.values()) {
				LOG.debug("Colonnes primaires : " + column.executeCommandQueryCreateSQL());
				
				if(this.autoIncrementValue) {
					sb.append(column.executeCommandQueryCreateSQL() + " PRIMARY KEY AUTOINCREMENT");
					sb.append(",\n");
				} else {
					sb.append(column.executeCommandQueryCreateSQL());
					sb.append(",\n");
				}
			}
			
			if(!this.autoIncrementValue) {
				sb.append("PRIMARY KEY(");
				
				for(ColumnORM column : this.columnsPrimaryKeys.values()) {
					sb.append(column.getName());
					sb.append(",");
				}
				
				//On supprime la dernière virgule
				sb.deleteCharAt(sb.length() - 1);
				
				sb.append("),\n");	
			}
		}
		
		//Puis la contrainte de clé étrangère si la table à une super classe
		if(this.tableParent != null) {
			sb.append("FOREIGN KEY(");
			
			for(ColumnORM column : this.tableParent.getColumnsPrimaryKeys().values()) {
				sb.append(column.getName());
				sb.append(",");
			}
			
			//On supprime la dernière virgule
			sb.deleteCharAt(sb.length() - 1);
			
			sb.append(") REFERENCES ");
			sb.append(this.tableParent.getName());
			sb.append("(");
			
			for(ColumnORM column : this.tableParent.getColumnsPrimaryKeys().values()) {
				sb.append(column.getName());
				sb.append(",");
			}
			
			//On supprime la dernière virgule
			sb.deleteCharAt(sb.length() - 1);
			sb.append(") ");
			
			//On ne peut référencé qu'une seule table
			sb.append(this.getConstraintOnDelete());
			sb.append(" ");
			sb.append(this.getConstraintOnUpdate());
			
			sb.append(",\n");
		}
		
		//On supprime la dernière virgule
		sb.deleteCharAt(sb.length() - 2);
		
		sb.append(");");
		
		return sb.toString();
	}
	

	@Override
	public String executeCommandQueryDeleteSQL() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("DROP TABLE IF EXISTS ");
		sb.append(this.name);
		sb.append(";");
		
		return sb.toString();
	}
	
	@Override
	public String getCreateCommandSQL(int size) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("INSERT INTO ");
		sb.append(this.name);
		sb.append(" VALUES(");
		
		for(int i=0; i<size; i++) {
			sb.append("?,");
		}
		
		//On supprime la dernière virgule
		sb.deleteCharAt(sb.length() - 1);
		
		sb.append(")");
		
		return sb.toString();
	}
	
	@Override
	public String getReadCommandSQL() {
		return "SELECT * FROM " + this.name;
	}

	@Override
	public String getReadCommandSQL(Map<? extends ColumnORM, Object> columnsWhere) {
		List<String> listFrom = new ArrayList<>();
		List<String> listWhere = new ArrayList<>();
		
		//On ajoute la table source dans le from
		String from = this.name + " " + this.name; // nom alias
		listFrom.add(from);
		
		for(ColumnORM column : columnsWhere.keySet()) {
			//Si la valeur est complexe (objet) dans ce cas il faut interroger à partir de la colonne étrangère
			//	- La table source
			//	- La table association
			//	- La table cible
			String where = "";
			if(column instanceof ColumnForeignKeyORM) {
				LOG.debug("Evaluation sur un objet complexe");
				
				ColumnForeignKeyORM columnForeign = (ColumnForeignKeyORM)column;
				
				TableORM tableAssoc = columnForeign.getTableAssociation();
				TableORM tableTarget = columnForeign.getTableTarget();
				
				//On ajoute les deux tables dans le from
				
				String tableAssocName = tableAssoc.getName();
				from = tableAssocName + " " + tableAssocName; // nom alias
				if(!listFrom.contains(from))
					listFrom.add(from);
				
				String tableTargetName = tableTarget.getName();
				from = tableTargetName + " " + tableTargetName; // nom alias
				if(!listFrom.contains(from))
					listFrom.add(from);
				
				//On récupère les valeurs clés primaires de l'objet
				
				for(ColumnPrimaryKeyORM columnPK : tableTarget.getColumnsPrimaryKeys().values()) {
					where = tableTargetName + "." + columnPK.getName() + " = ?";
					LOG.debug("Where clé primaire : " + where);
					if(!listWhere.contains(where))
						listWhere.add(where);
				}
				
				//On effectue la jointure de la table source vers la table association
				
				int i=0;
				for(ColumnORM columnPK : tableAssoc.getColumnsPrimaryKeys().values()) {
					ColumnORM columnPKSource = (ColumnORM)this.columnsPrimaryKeys.values().toArray()[i];
					String columnPKSourceName = this.name + "." + columnPKSource.getName();
					
					where = tableAssocName + "." + columnPK.getName() + " = " + columnPKSourceName;
					LOG.debug("Where jointure entre assoc et source : " + where);
					if(!listWhere.contains(where))
						listWhere.add(where);
					i++;
				}
				
				//On effectue la jointure de la table association vers la table target
				
				i=0;
				for(ColumnORM columnPK : tableAssoc.getColumnsForeignKeys().values()) {
					ColumnORM columnPKTarget = (ColumnORM)tableTarget.getColumnsPrimaryKeys().values().toArray()[i];
					String columnPKTargetName = tableTargetName + "." + columnPKTarget.getName();
					
					where = tableAssocName + "." + columnPK.getName() + " = " + columnPKTargetName;
					LOG.debug("Where jointure entre assoc et target : " + where);
					if(!listWhere.contains(where))
						listWhere.add(where);
					i++;
				}
				
			//Si la valeur est primitive
			} else {
				LOG.debug("Evaluation sur un type primitive");
				where = this.name + "." + column.getName() + " = ?";
				listWhere.add(where);
			}
		}
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT ");
		String select = this.name + ".*";
		sb.append(select);
		sb.append("\n");
		
		sb.append("FROM ");
		String froms = String.join(" , ", listFrom);
		sb.append(froms);
		sb.append("\n");
		
		sb.append("WHERE ");
		String wheres = String.join(" AND\n", listWhere);
		sb.append(wheres);
		
		return sb.toString();
	}
	
	@Override
	public String getReadCommandSQL(Map<String, ? extends ColumnORM> columnsSelect, 
			Map<? extends ColumnORM, Object> columnsWhere) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT ");
		
		for(ColumnORM column : columnsSelect.values()) {
			sb.append(column.getName());
			sb.append(",");
		}
		
		//On supprime la dernière virgule
		sb.deleteCharAt(sb.length()-1);
		
		sb.append(" FROM ");
		sb.append(this.name);
		sb.append(" WHERE ");
		
		for(ColumnORM column : columnsWhere.keySet()) {
			sb.append(column.getName());
			sb.append(" = ? AND ");
		}
		
		//On supprime le dernier AND
		sb.delete(sb.length()-5, sb.length());
		
		return sb.toString();
	}
	

	@Override
	public String getUpdateCommandSQL(Map<String, ? extends ColumnORM> columnsWhere) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("UPDATE ");
		sb.append(this.name);
		sb.append(" SET \n");
		
		//Pour chaque colonne modifiable
		for(ColumnORM column : this.columns.values()) {
			sb.append(column.getName() + " = ?,\n");
		}
		
		//On supprime la dernière virgule
		sb.deleteCharAt(sb.length() - 2);
		
		sb.append(" WHERE ");
		
		for(ColumnORM column : columnsWhere.values()) {
			sb.append(column.getName());
			sb.append(" = ? AND ");
		}
		
		//On supprime le dernier AND
		sb.delete(sb.length()-5, sb.length());
		
		return sb.toString();
	}
	
	@Override
	public String getDeleteCommandSQL(Map<String, ? extends ColumnORM> columnsWhere) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("DELETE FROM ");
		sb.append(this.name);
		sb.append(" WHERE ");
		
		for(ColumnORM column : columnsWhere.values()) {
			sb.append(column.getName());
			sb.append(" = ? AND ");
		}
		
		//On supprime le dernier AND
		sb.delete(sb.length()-5, sb.length());
		
		return sb.toString();
	}
	
	@Override
	public String getConstraintOnCreate() {
		return "ON CREATE CASCADE";
	}

	@Override
	public String getConstraintOnRead() {
		return "ON READ CASCADE";
	}

	@Override
	public String getConstraintOnUpdate() {
		return "ON UPDATE CASCADE";
	}

	@Override
	public String getConstraintOnDelete() {
		return "ON DELETE CASCADE";
	}
}
