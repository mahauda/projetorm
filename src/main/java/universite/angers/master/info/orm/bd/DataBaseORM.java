package universite.angers.master.info.orm.bd;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import universite.angers.master.info.orm.bd.column.ColumnForeignKeyORM;
import universite.angers.master.info.orm.bd.column.ColumnORM;
import universite.angers.master.info.orm.bd.column.ColumnPrimaryKeyORM;
import universite.angers.master.info.orm.bd.table.TableORM;
import universite.angers.master.info.orm.util.UtilORM;

/**
 * Classe qui rescence toutes les tables de la base de données pour effectuer
 * des opérations CRUD
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class DataBaseORM implements Executable, Queryable {

	/**
	 * Log de classe DataBaseORM
	 */
	private static final Logger LOG = Logger.getLogger(DataBaseORM.class);

	/**
	 * La session pour se connecter à la BD
	 */
	protected SessionORM session;

	/**
	 * Les tables de la BD
	 */
	protected Map<String, TableORM> tables;

	public DataBaseORM(SessionORM session, List<Class<?>> objects) {
		this.session = session;
		this.session.setDataBase(this);
		LOG.debug("init session : " + this.session);

		this.tables = this.initTables(objects);
		LOG.debug("init tables : " + this.tables);

		boolean initTablesAssoc = this.initTablesAssociation();
		LOG.debug("init tables assoc : " + initTablesAssoc);

		boolean initTablesParent = this.initTablesParents();
		LOG.debug("init tables parents : " + initTablesParent);
	}

	/**
	 * Permet de créer une nouvelle table
	 * 
	 * @param name
	 * @param columns
	 * @param columnsPrimaryKeys
	 * @param columnsForeignKeys
	 * @return
	 */
	public abstract TableORM newTableORM(String name, Map<String, ColumnORM> columns,
			Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys, Map<String, ColumnForeignKeyORM> columnsForeignKeys);

	/**
	 * Permet de créer une nouvelle table
	 * 
	 * @param object
	 * @return
	 */
	public abstract TableORM newTableORM(Class<?> object);

	/**
	 * Initialise les tables selon les classes
	 * 
	 * @param objects
	 * @return
	 */
	private Map<String, TableORM> initTables(List<Class<?>> objects) {
		LOG.debug("initTables objects class ORM : " + objects);
		Map<String, TableORM> tables = new HashMap<>();

		if (objects == null)
			return tables;
		if (objects.isEmpty())
			return tables;

		for (Class<?> object : objects) {
			LOG.debug("initTables object class ORM : " + object);
			if (object == null)
				continue;

			TableORM table = this.newTableORM(object);
			LOG.debug("initTables table ORM : " + table);

			if (table == null)
				continue;

			if (!tables.containsKey(object.getName())) {
				tables.put(object.getName(), table);
				LOG.debug("initTables ajout de la table ORM : " + object.getName());
			}
		}

		return tables;
	}

	/**
	 * Méthode qui permet de créer les tables association et de les enregistrer
	 */
	private boolean initTablesAssociation() {
		for (TableORM table : this.tables.values()) {
			LOG.debug("initTablesAssoc table ORM : " + table);
			LOG.debug("initTablesAssoc columns foreign key ORM : " + table.getColumnsForeignKeys());

			if (table.getColumnsForeignKeys() == null)
				return false;

			for (ColumnForeignKeyORM columnForeignKey : table.getColumnsForeignKeys().values()) {
				LOG.debug("initTablesAssoc columnForeignKey : " + columnForeignKey);
				LOG.debug("initTablesAssoc columnForeignKey clazz : " + columnForeignKey.getClazz());

				if (columnForeignKey.isPrimitiveValue()) {
					TableORM tableTarget = this.newTableORM("primitive", new HashMap<>(), new HashMap<>(),
							new HashMap<>());
					ColumnPrimaryKeyORM column = tableTarget.newColumnPrimaryKeyORM(null, int.class, "value",
							new String[] {}, null);
					tableTarget.getColumnsPrimaryKeys().put("value", column);

					columnForeignKey.setTableTarget(tableTarget);
					LOG.debug("initTablesAssoc table target : " + tableTarget);

					TableORM tableAssociation = this.createTableAssociation(columnForeignKey);
					columnForeignKey.setTableAssociation(tableAssociation);
					LOG.debug("initTablesAssoc table association : " + tableAssociation);

				} else {
					if (columnForeignKey.getClazz() == null)
						return false;

					if (this.tables.containsKey(columnForeignKey.getClazz().getName())) {

						TableORM tableTarget = this.tables.get(columnForeignKey.getClazz().getName());
						columnForeignKey.setTableTarget(tableTarget);
						LOG.debug("initTablesAssoc table target : " + tableTarget);

						TableORM tableAssociation = this.createTableAssociation(columnForeignKey);
						columnForeignKey.setTableAssociation(tableAssociation);
						LOG.debug("initTablesAssoc table association : " + tableAssociation);
					} else
						return false;
				}
			}
		}

		return true;
	}

	/**
	 * Méthode qui permet d'associer des tables hérités à d'éventuels tables parents
	 */
	private boolean initTablesParents() {
		for (TableORM table : this.tables.values()) {
			Class<?> clazz = table.getObject();
			LOG.debug("initTablesParents class : " + clazz);
			LOG.debug("initTablesParents super Class : " + clazz.getSuperclass());

			// Si l'objet provient d'une classe hérité autre que Object
			if (clazz.getSuperclass() != null && !clazz.getSuperclass().isAssignableFrom(Object.class)) {
				// Dans ce cas on récupère la table parent si enregistré
				if (this.tables.containsKey(clazz.getSuperclass().getName())) {
					TableORM parent = this.tables.get(clazz.getSuperclass().getName());
					LOG.debug("initTablesParents table parent : " + parent);

					// Puis on l'affecte dans la table héritée
					table.setTableParent(parent);

					// On ajoute la table héritée dans la table parent
					if (!parent.getTablesChildrens().containsKey(table.getObject().getName()))
						parent.getTablesChildrens().put(table.getObject().getName(), table);

				} else
					return false;
			}
		}

		return true;
	}

	/**
	 * Méthode qui permet de créer une table association à partir d'une colonne
	 * étrangère
	 * 
	 * @param columnForeignKey
	 * @return
	 */
	private TableORM createTableAssociation(ColumnForeignKeyORM columnForeignKey) {
		LOG.debug("createTableAssoc columnForeignKey : " + columnForeignKey);
		if (columnForeignKey == null)
			return null;
		if (columnForeignKey.getTableSource() == null)
			return null;

		String tableName = columnForeignKey.getTableSource().getName() + "_" + columnForeignKey.getName();
		LOG.debug("createTableAssoc table nom : " + tableName);
		if (UtilORM.isNullOrEmpty(tableName))
			return null;

		Map<String, ColumnORM> tableColumns = this.getColumnsTableAssociation(columnForeignKey);
		LOG.debug("createTableAssoc table colonnes : " + tableColumns);
		if (tableColumns == null)
			return null;

		return this.initTableAssociation(columnForeignKey, tableName, tableColumns);
	}

	/**
	 * Construit une table association à partir d'une colonne clé étrangère
	 * 
	 * @param columnForeignKey
	 * @param tableName
	 * @return
	 */
	private Map<String, ColumnORM> getColumnsTableAssociation(ColumnForeignKeyORM columnForeignKey) {
		Map<String, ColumnORM> columns = new HashMap<>();

		// Champ clé primaire premiere table
		int i = 1;

		for (ColumnORM column : columnForeignKey.getTableSource().getColumnsPrimaryKeys().values()) {
			String columnNameMap = columnForeignKey.getTableSource().getName() + "_" + columnForeignKey.getName() + "_"
					+ column.getName() + "_" + i;
			ColumnORM columnMap = columnForeignKey.getTableSource().newColumnORM(column.getField(), column.getClazz(),
					columnNameMap, new String[] {}, null);
			columns.put(columnNameMap, columnMap);
			i++;
		}

		// Champ clé primaire seconde table

		for (ColumnORM column : columnForeignKey.getTableTarget().getColumnsPrimaryKeys().values()) {
			String columnNameMap = columnForeignKey.getTableTarget().getName() + "_" + columnForeignKey.getName() + "_"
					+ column.getName() + "_" + i;
			ColumnORM columnMap = columnForeignKey.getTableTarget().newColumnORM(column.getField(), column.getClazz(),
					columnNameMap, new String[] {}, null);
			columns.put(columnNameMap, columnMap);
			i++;
		}

		return columns;
	}

	/**
	 * Construit une table association à partir des définitions des colonnes
	 * 
	 * @param columnForeignKey
	 * @param tableName
	 * @param tableColumns
	 * @return
	 */
	private TableORM initTableAssociation(ColumnForeignKeyORM columnForeignKey, String tableName,
			Map<String, ColumnORM> tableColumns) {

		// Champ clé primaire premiere table

		int i = 1;

		Map<String, ColumnPrimaryKeyORM> columnsPrimaryKey = new HashMap<>();

		for (ColumnORM column : columnForeignKey.getTableSource().getColumnsPrimaryKeys().values()) {
			String columnNameMap = columnForeignKey.getTableSource().getName() + "_" + columnForeignKey.getName() + "_"
					+ column.getName() + "_" + i;
			ColumnPrimaryKeyORM columnMap = columnForeignKey.getTableSource().newColumnPrimaryKeyORM(column.getField(),
					column.getClazz(), columnNameMap, new String[] {}, null);
			columnsPrimaryKey.put(columnNameMap, columnMap);
			i++;
		}

		// Champ clé primaire seconde table

		Map<String, ColumnForeignKeyORM> columnsForeignKey = new HashMap<>();

		for (ColumnORM column : columnForeignKey.getTableTarget().getColumnsPrimaryKeys().values()) {
			String columnNameMap = columnForeignKey.getTableTarget().getName() + "_" + columnForeignKey.getName() + "_"
					+ column.getName() + "_" + i;
			ColumnForeignKeyORM columnMap = columnForeignKey.getTableTarget().newColumnForeignKeyORM(column.getField(),
					column.getClazz(), columnNameMap, new String[] {}, null, null, null, null);
			columnsForeignKey.put(columnNameMap, columnMap);
			i++;
		}

		return this.newTableORM(tableName, tableColumns, columnsPrimaryKey, columnsForeignKey);
	}

	/**
	 * Nettoye toutes les instances d'objets dans les tables
	 */
	private void clear() {
		for (TableORM table : this.tables.values()) {
			table.clear();
		}
	}
	
	@Override
	public String executeCommandQueryCreateSQL() {
		StringBuilder sb = new StringBuilder();

		for (TableORM table : this.tables.values()) {
			for (ColumnForeignKeyORM columnForeignKey : table.getColumnsForeignKeys().values()) {
				sb.append(columnForeignKey.executeCommandQueryCreateSQLTableAssociation());
				sb.append("\n");
			}
			sb.append(table.executeCommandQueryCreateSQL());
			sb.append("\n");
		}

		return sb.toString();
	}

	@Override
	public String executeCommandQueryDeleteSQL() {
		StringBuilder sb = new StringBuilder();

		for (TableORM table : this.tables.values()) {
			for (ColumnForeignKeyORM columnForeignKey : table.getColumnsForeignKeys().values()) {
				sb.append(columnForeignKey.executeCommandQueryDeleteSQLTableAssociation());
				sb.append("\n");
			}
			sb.append(table.executeCommandQueryDeleteSQL());
			sb.append("\n");
		}

		return sb.toString();
	}

	/**
	 * Création de toutes les tables dans la BD
	 * @return
	 */
	public synchronized boolean create() {
		// On exécute la création de toutes les tables une par une
		// afin de détecter les erreurs
		for (TableORM table : this.tables.values()) {
			LOG.debug("initialize table : " + table);

			// On crée d'abord les tables associations de chaque clé étrangère définis
			for (ColumnForeignKeyORM columnForeignKey : table.getColumnsForeignKeys().values()) {
				LOG.debug("initialize Column Foreign Key execute create table association : " + columnForeignKey);

				String sqlCommandCreate = columnForeignKey.executeCommandQueryCreateSQLTableAssociation();
				LOG.debug("initialize sql command create : " + sqlCommandCreate);

				if (UtilORM.isNullOrEmpty(sqlCommandCreate))
					return false;

				if (!this.session.executeQuery(sqlCommandCreate))
					return false;
			}

			if (!this.session.create(table))
				return false;
		}

		return true;
	}

	/**
	 * Suppression de toutes les tables dans la BD
	 * @return
	 */
	public synchronized boolean delete() {
		// On exécute la supression de toutes les tables une par une
		// afin de détecter les erreurs
		for (TableORM table : this.tables.values()) {
			LOG.debug("initialize table : " + table);

			// On supprime d'abord les tables associations de chaque clé étrangère définis
			for (ColumnForeignKeyORM columnForeignKey : table.getColumnsForeignKeys().values()) {
				LOG.debug("initialize Column Foreign Key execute create table association : " + columnForeignKey);

				String sqlCommandDelete = columnForeignKey.executeCommandQueryDeleteSQLTableAssociation();
				LOG.debug("initialize sql command delete : " + sqlCommandDelete);

				if (UtilORM.isNullOrEmpty(sqlCommandDelete)) continue;

				this.session.executeQuery(sqlCommandDelete);
			}

			this.session.delete(table);
		}

		return true;
	}

	/**
	 * Ouverture de la BD
	 * 
	 * @return
	 */
	public synchronized boolean open() {
		return this.session.open();
	}

	/**
	 * Fermeture de la BD
	 * 
	 * @return
	 */
	public synchronized boolean close() {
		return this.session.close();
	}

	/**
	 * Enregistrement d'un objet dans la BD
	 * 
	 * @param object
	 * @return
	 */
	@Override
	public synchronized boolean create(Object object) {
		if (object == null)
			return false;
		if (!this.tables.containsKey(object.getClass().getName()))
			return false;

		TableORM table = this.tables.get(object.getClass().getName());
		LOG.debug("Table create : " + table);

		if (table == null)
			return false;
		boolean create = this.session.executeCreate(table, object);
		this.clear();

		return create;
	}

	/**
	 * Lecture de tous les objets d'une table
	 * 
	 * @param tableName
	 * @return
	 */
	@Override
	public synchronized Collection<Object> read(Class<?> type) {
		LOG.debug("Type read : " + type);

		if (type == null)
			return Collections.emptyList();
		if (!this.tables.containsKey(type.getName()))
			return Collections.emptyList();

		TableORM table = this.tables.get(type.getName());
		LOG.debug("Table read : " + table);

		if (table == null)
			return Collections.emptyList();

		return this.session.executeRead(table, null, null);
	}

	@Override
	public synchronized Collection<Object> read(Class<?> type, Map<String, Object> where) {
		LOG.debug("Type read : " + type);
		LOG.debug("Where read : " + where);

		if (where == null)
			return this.read(type);
		if (type == null)
			return Collections.emptyList();
		if (!this.tables.containsKey(type.getName()))
			return Collections.emptyList();

		TableORM table = this.tables.get(type.getName());
		LOG.debug("Table read : " + table);

		if (table == null)
			return Collections.emptyList();

		Map<ColumnORM, Object> columnsWhere = new HashMap<>();
		for (Entry<String, Object> columnValue : where.entrySet()) {
			String column = columnValue.getKey();

			ColumnORM columnORM = this.getColumnORM(table, column);
			if (columnORM == null)
				return Collections.emptyList();

			columnsWhere.put(columnORM, columnValue.getValue());
		}

		return this.session.executeRead(table, null, columnsWhere);
	}

	private ColumnORM getColumnORM(TableORM table, String column) {
		if (!table.getColumns().containsKey(column)) {
			if (!table.getColumnsPrimaryKeys().containsKey(column)) {
				if (!table.getColumnsForeignKeys().containsKey(column)) {
					// On rechercher de facon récursive dans les tables parents
					if (table.getTableParent() != null) {
						return this.getColumnORM(table.getTableParent(), column);
					} else
						return null;
				} else
					return table.getColumnsForeignKeys().get(column);
			} else
				return table.getColumnsPrimaryKeys().get(column);
		} else
			return table.getColumns().get(column);
	}

	/**
	 * Mise à jour d'un objet
	 * 
	 * @param object
	 * @return
	 */
	@Override
	public synchronized boolean update(Object object) {
		if (object == null)
			return false;
		if (!this.tables.containsKey(object.getClass().getName()))
			return false;

		TableORM table = this.tables.get(object.getClass().getName());
		LOG.debug("Table update : " + table);

		if (table == null)
			return false;

		boolean update = this.session.executeUpdate(table, object);
		this.clear();

		return update;
	}

	/**
	 * Suppression d'un objet
	 * 
	 * @param object
	 * @return
	 */
	@Override
	public synchronized boolean delete(Object object) {
		if (object == null)
			return false;
		if (!this.tables.containsKey(object.getClass().getName()))
			return false;

		TableORM table = this.tables.get(object.getClass().getName());
		LOG.debug("Table create : " + table);

		if (table == null)
			return false;
		boolean delete = this.session.executeDelete(table, object);
		this.clear();

		return delete;
	}

	/**
	 * @return the session
	 */
	public SessionORM getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(SessionORM session) {
		this.session = session;
	}

	/**
	 * @return the tables
	 */
	public Map<String, TableORM> getTables() {
		return tables;
	}

	/**
	 * @param tables the tables to set
	 */
	public void setTables(Map<String, TableORM> tables) {
		this.tables = tables;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((session == null) ? 0 : session.hashCode());
		result = prime * result + ((tables == null) ? 0 : tables.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataBaseORM other = (DataBaseORM) obj;
		if (session == null) {
			if (other.session != null)
				return false;
		} else if (!session.equals(other.session))
			return false;
		if (tables == null) {
			if (other.tables != null)
				return false;
		} else if (!tables.equals(other.tables))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DataBaseORM [session=" + session + ", tables=" + tables + "]";
	}
}
