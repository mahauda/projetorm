package universite.angers.master.info.orm.bd;

import java.util.Collection;
import java.util.Map;

/**
 * Interface qui permet d'interroger une base de données
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Queryable {
	
	/**
	 * Insérer un objet quelconque
	 * @param object
	 * @return vrai si inséré. Faux dans le cas contraire
	 */
	public boolean create(Object object);
	
	/**
	 * Récupérer une liste d'objets pour un type donnée
	 * @param type
	 * @return
	 */
	public Collection<Object> read(Class<?> type);
	
	/**
	 * Récupérer une liste d'objets sous conditions pour un type donnée
	 * @param type
	 * @param where
	 * @return
	 */
	public Collection<Object> read(Class<?> type, Map<String, Object> where);
	
	/**
	 * Mise à jour d'un objet quelconque
	 * @param object
	 * @return vrai si maj. Faux dans le cas contraire
	 */
	public boolean update(Object object);
	
	/**
	 * Suppression d'un objet quelconque
	 * @param object
	 * @return vrai si supprimé. Faux dans le cas contraire
	 */
	public boolean delete(Object object);
}
