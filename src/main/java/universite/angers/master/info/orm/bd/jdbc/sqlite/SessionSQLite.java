package universite.angers.master.info.orm.bd.jdbc.sqlite;

import java.io.File;
import java.sql.Driver;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.sqlite.SQLiteConfig;
import universite.angers.master.info.orm.bd.jdbc.SessionJdbcORM;

/**
 * Classe session SQLite
 * Source : http://code-know-how.blogspot.com/2011/10/how-to-enable-foreign-keys-in-sqlite3.html
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class SessionSQLite extends SessionJdbcORM {

	private static final Logger LOG = Logger.getLogger(SessionSQLite.class);
	
	public SessionSQLite(boolean create, String name, String path, String provider, String host, String port) {
		super(create, name, path, provider, host, port);
	}

	@Override
	public String initURL() {
		//jdbc:sqlite:C:/sqlite/db/chinook.db
		return String.format("%s:%s/%s", this.provider, this.path, this.name);
	}

	@Override
	public Driver initDriver() {
		return new org.sqlite.JDBC();
	}

	@Override
	public Properties initProperties() {
		//La contrainte de clé étrangère n'est pas activé par défaut
		//Il faut l'activer dans les propriétés
		SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true);
        return config.toProperties();
	}

	@Override
	public boolean create() {
		boolean create = new File(this.path).mkdirs();
		LOG.debug("Create file BD : " + create);
		
		return create;

	}

	@Override
	public boolean delete() {
		boolean delete = new File(this.path + "/" + this.name).delete();
		LOG.debug("Delete file BD : " + delete);
		
		return delete;
	}
	
	@Override
	public String toString() {
		return "SessionSQLite [create=" + create + ", name=" + name + ", path=" + path + ", provider=" + provider
				+ ", host=" + host + ", port=" + port + ", userName=" + userName + ", password=" + password + "]";
	}
}