package universite.angers.master.info.orm.bd.jdbc.sqlite;

import java.util.List;
import java.util.Map;
import universite.angers.master.info.orm.bd.DataBaseORM;
import universite.angers.master.info.orm.bd.column.ColumnForeignKeyORM;
import universite.angers.master.info.orm.bd.column.ColumnORM;
import universite.angers.master.info.orm.bd.column.ColumnPrimaryKeyORM;
import universite.angers.master.info.orm.bd.table.TableORM;

/**
 * Classe BD SQLite
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class DataBaseSQLite extends DataBaseORM {

	public DataBaseSQLite(SessionSQLite session, List<Class<?>> objects) {
		super(session, objects);
	}

	@Override
	public TableORM newTableORM(String name, Map<String, ColumnORM> columns,
			Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys, Map<String, ColumnForeignKeyORM> columnsForeignKeys) {
		return new TableSQLite(name, columns, columnsPrimaryKeys, columnsForeignKeys);
	}

	@Override
	public TableORM newTableORM(Class<?> object) {
		return new TableSQLite(object);
	}

	@Override
	public String toString() {
		return "DataBaseSQLite [session=" + session + ", tables=" + tables + "]";
	}
}
