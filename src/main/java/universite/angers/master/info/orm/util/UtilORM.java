package universite.angers.master.info.orm.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import universite.angers.master.info.orm.bd.annotation.PrimaryKey;

/**
 * Classe utilitaire pour l'ORM
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class UtilORM {

	/**
	 * Constructeur afin d'empecher l'instanciation de la classe
	 */
	private UtilORM() {
		
	}
	
	/**
	 * Vérifier si une chaine est vide
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.trim().isEmpty())
            return false;
        return true;
    }
	
	/**
	 * Récupérer récursivement toutes les classes hérités d'un type
	 * @param type
	 * @param reverse
	 * @return
	 */
	public static List<Class<?>> getAllDeclaredClass(Class<?> type, boolean reverse) {
		List<Class<?>> types = getAllDeclaredClass(new ArrayList<Class<?>>(), type);
		if(reverse) {
			Collections.reverse(types);
			return types;
		} else return types;
	}
	
	/**
	 * Récupérer récursivement toutes les classes hérités d'un type
	 * @param types
	 * @param type
	 * @return
	 */
    private static List<Class<?>> getAllDeclaredClass(List<Class<?>> types, Class<?> type) {
    	types.add(type);

        if(type.getSuperclass() != null && !type.getSuperclass().isAssignableFrom(Object.class)) {
        	getAllDeclaredClass(types, type.getSuperclass());
        }

        return types;
    }
	
    /**
	 * Récupérer récursivement tous les champs déclarées, même héritées d'un objet
	 * @param object
	 * @param annotation
	 * @return
	 */
	public static List<Field> getAllDeclaredField(Class<?> object) {
		return getAllDeclaredField(new ArrayList<Field>(), object);
	}
    
	/**
	 * Récupérer récursivement tous les champs déclarées d'une annotation, même héritées d'un objet
	 * @param object
	 * @param annotation
	 * @return
	 */
	public static List<Field> getAllDeclaredField(Class<?> object, Class<? extends Annotation> annotation) {
		return getAllDeclaredField(new ArrayList<Field>(), object, annotation);
	}
	
	/**
	 * Récupérer récursivement tous les champs déclarées d'une annotation, même héritées d'un objet
	 * @param fields
	 * @param type
	 * @param annotation
	 * @return
	 */
    private static List<Field> getAllDeclaredField(List<Field> fields, Class<?> type, Class<? extends Annotation> annotation) {
        for(Field field : type.getDeclaredFields()) {
        	if(field.isAnnotationPresent(annotation))
        		fields.add(field);
        }

        //On ne récupère que les clés primaires dans les superclasses
        if(type.getSuperclass() != null && PrimaryKey.class.isAssignableFrom(annotation)) {
            getAllDeclaredField(fields, type.getSuperclass(), annotation);
        }

        return fields;
    }
    
    /**
	 * Récupérer récursivement tous les champs déclarées d'une annotation, même héritées d'un objet
	 * @param fields
	 * @param type
	 * @param annotation
	 * @return
	 */
    private static List<Field> getAllDeclaredField(List<Field> fields, Class<?> type) {
        for(Field field : type.getDeclaredFields()) {
        	fields.add(field);
        }

        //On ne récupère que les clés primaires dans les superclasses
        if(type.getSuperclass() != null) {
            getAllDeclaredField(fields, type.getSuperclass());
        }

        return fields;
    }
}
