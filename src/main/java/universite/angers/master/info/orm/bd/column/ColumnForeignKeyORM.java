package universite.angers.master.info.orm.bd.column;

import java.lang.reflect.Field;
import java.util.Map;
import org.apache.log4j.Logger;

import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.table.TableORM;

/**
 * Classe qui permet d'identifier les colonnes clés étrangères entre deux objets (tables). Elle référencent :
 * - La table source de l'objet source
 * - La table cible de l'objet cible
 * - La table association représentant le lien entre ces deux objets
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class ColumnForeignKeyORM extends ColumnORM {

	/**
	 * Log de classe ColumnForeignKeyORM
	 */
	private static final Logger LOG = Logger.getLogger(ColumnForeignKeyORM.class);
	
	/**
	 * La table source de l'objet source
	 */
	protected TableORM tableSource;
	
	/**
	 * La table cible de l'objet cible
	 */
	protected TableORM tableTarget;
	
	/**
	 * La table association représentant le lien entre ces deux objets
	 */
	protected TableORM tableAssociation;
	
	/**
	 * Doit-on créer en cascade les objets complexes ?
	 */
	protected boolean onCreateCascade;
	
	/**
	 * Doit-on lire en cascade les objets complexes ?
	 */
	protected boolean onReadCascade;
	
	/**
	 * Doit-on maj en cascade les objets complexes ?
	 */
	protected boolean onUpdateCascade;
	
	/**
	 * Doit-on supprimer en cascade les objets complexes ?
	 */
	protected boolean onDeleteCascade;
	
	public ColumnForeignKeyORM(Field field, Class<?> clazz, String name, String[] constraints, 
			TableORM tableSource, TableORM tableTarget, TableORM tableAssociation, Object defaultValue) {
		super(field, clazz, name, constraints, defaultValue);
		
		this.tableSource = tableSource;
		LOG.debug("Table source : " + this.tableSource);
		
		this.tableTarget = tableTarget;
		LOG.debug("Table target : " + this.tableTarget);
		
		this.tableAssociation = tableAssociation;
		LOG.debug("Table association : " + this.tableAssociation);
		
		this.initCascade();
	}
	
	public ColumnForeignKeyORM(Field field, TableORM tableSource, TableORM tableTarget, TableORM tableAssociation) {
		super(field);
		
		this.tableSource = tableSource;
		LOG.debug("Table source : " + this.tableSource);
		
		this.tableTarget = tableTarget;
		LOG.debug("Table target : " + this.tableTarget);
		
		this.tableAssociation = tableAssociation;
		LOG.debug("Table association : " + this.tableAssociation);
		
		this.initCascade();
	}
	
	private void initCascade() {
		if(this.field != null && this.field.isAnnotationPresent(ForeignKey.class)) {
			ForeignKey fk = this.field.getDeclaredAnnotation(ForeignKey.class);
			this.onCreateCascade = fk.onCreateCascade();
			this.onReadCascade = fk.onReadCascade();
			this.onUpdateCascade = fk.onUpdateCascade();
			this.onDeleteCascade = fk.onDeleteCascade();
		} else {
			this.onCreateCascade = false;
			this.onReadCascade = false;
			this.onUpdateCascade = false;
			this.onDeleteCascade = false;
		}
	}
	
	/**
	 * Commande qui permet de créer la table association grâce aux des tables (source+target)
	 * @return la commande
	 */
	public abstract String executeCommandQueryCreateSQLTableAssociation();
	
	/**
	 * Commande qui permet de supprimer la table association grâce aux des tables (source+target)
	 * @return la commande
	 */
	public abstract String executeCommandQueryDeleteSQLTableAssociation();
	
	/**
	 * Permet de créer une table
	 * @param name
	 * @param columns
	 * @param columnsPrimaryKeys
	 * @param columnsForeignKeys
	 * @return
	 */
	public abstract TableORM newTableORM(String name, Map<String, ColumnORM> columns, Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys,
			Map<String, ColumnForeignKeyORM> columnsForeignKeys);

	/**
	 * @return the tableSource
	 */
	public TableORM getTableSource() {
		return tableSource;
	}

	/**
	 * @param tableSource the tableSource to set
	 */
	public void setTableSource(TableORM tableSource) {
		this.tableSource = tableSource;
	}

	/**
	 * @return the tableTarget
	 */
	public TableORM getTableTarget() {
		return tableTarget;
	}

	/**
	 * @param tableTarget the tableTarget to set
	 */
	public void setTableTarget(TableORM tableTarget) {
		this.tableTarget = tableTarget;
	}

	/**
	 * @return the tableAssociation
	 */
	public TableORM getTableAssociation() {
		return tableAssociation;
	}

	/**
	 * @param tableAssociation the tableAssociation to set
	 */
	public void setTableAssociation(TableORM tableAssociation) {
		this.tableAssociation = tableAssociation;
	}

	/**
	 * @return the onCreateCascade
	 */
	public boolean isOnCreateCascade() {
		return onCreateCascade;
	}

	/**
	 * @param onCreateCascade the onCreateCascade to set
	 */
	public void setOnCreateCascade(boolean onCreateCascade) {
		this.onCreateCascade = onCreateCascade;
	}

	/**
	 * @return the onReadCascade
	 */
	public boolean isOnReadCascade() {
		return onReadCascade;
	}

	/**
	 * @param onReadCascade the onReadCascade to set
	 */
	public void setOnReadCascade(boolean onReadCascade) {
		this.onReadCascade = onReadCascade;
	}

	/**
	 * @return the onUpdateCascade
	 */
	public boolean isOnUpdateCascade() {
		return onUpdateCascade;
	}

	/**
	 * @param onUpdateCascade the onUpdateCascade to set
	 */
	public void setOnUpdateCascade(boolean onUpdateCascade) {
		this.onUpdateCascade = onUpdateCascade;
	}

	/**
	 * @return the onDeleteCascade
	 */
	public boolean isOnDeleteCascade() {
		return onDeleteCascade;
	}

	/**
	 * @param onDeleteCascade the onDeleteCascade to set
	 */
	public void setOnDeleteCascade(boolean onDeleteCascade) {
		this.onDeleteCascade = onDeleteCascade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (onCreateCascade ? 1231 : 1237);
		result = prime * result + (onDeleteCascade ? 1231 : 1237);
		result = prime * result + (onReadCascade ? 1231 : 1237);
		result = prime * result + (onUpdateCascade ? 1231 : 1237);
		result = prime * result + ((tableAssociation == null) ? 0 : tableAssociation.hashCode());
		result = prime * result + ((tableSource == null) ? 0 : tableSource.hashCode());
		result = prime * result + ((tableTarget == null) ? 0 : tableTarget.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColumnForeignKeyORM other = (ColumnForeignKeyORM) obj;
		if (onCreateCascade != other.onCreateCascade)
			return false;
		if (onDeleteCascade != other.onDeleteCascade)
			return false;
		if (onReadCascade != other.onReadCascade)
			return false;
		if (onUpdateCascade != other.onUpdateCascade)
			return false;
		if (tableAssociation == null) {
			if (other.tableAssociation != null)
				return false;
		} else if (!tableAssociation.equals(other.tableAssociation))
			return false;
		if (tableSource == null) {
			if (other.tableSource != null)
				return false;
		} else if (!tableSource.equals(other.tableSource))
			return false;
		if (tableTarget == null) {
			if (other.tableTarget != null)
				return false;
		} else if (!tableTarget.equals(other.tableTarget))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ColumnForeignKeyORM [tableSource=" + tableSource + ", tableTarget=" + tableTarget
				+ ", tableAssociation=" + tableAssociation + ", onCreateCascade=" + onCreateCascade + ", onReadCascade="
				+ onReadCascade + ", onUpdateCascade=" + onUpdateCascade + ", onDeleteCascade=" + onDeleteCascade + "]";
	}
}
