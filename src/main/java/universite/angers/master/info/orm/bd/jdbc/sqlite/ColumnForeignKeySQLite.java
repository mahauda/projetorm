package universite.angers.master.info.orm.bd.jdbc.sqlite;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import universite.angers.master.info.orm.bd.column.ColumnForeignKeyORM;
import universite.angers.master.info.orm.bd.column.ColumnORM;
import universite.angers.master.info.orm.bd.column.ColumnPrimaryKeyORM;
import universite.angers.master.info.orm.bd.table.TableORM;

/**
 * Classe colonne clé étrangère SQLite
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ColumnForeignKeySQLite extends ColumnForeignKeyORM {
	
	public ColumnForeignKeySQLite(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue,
			TableORM tableSource, TableORM tableTarget, TableORM tableAssociation) {
		super(field, clazz, name, constraints, tableSource, tableTarget, tableAssociation, defaultValue);
	}
	
	public ColumnForeignKeySQLite(Field field, TableORM tableSource, TableORM tableTarget, TableORM tableAssociation) {
		super(field, tableSource, tableTarget, tableAssociation);
	}

	@Override
	public TableORM newTableORM(String name, Map<String, ColumnORM> columns,
			Map<String, ColumnPrimaryKeyORM> columnsPrimaryKeys, Map<String, ColumnForeignKeyORM> columnsForeignKeys) {
		return new TableSQLite(name, columns, columnsPrimaryKeys, columnsForeignKeys);
	}
	
	@Override
	public String getTypeBoolean() {
		return "INTEGER";
	}
	
	@Override
	public String getTypeByte() {
		return "INTEGER";
	}
	
	@Override
	public String getTypeInteger() {
		return "INTEGER";
	}
	
	@Override
	public String getTypeShort() {
		return "REAL";
	}
	
	@Override
	public String getTypeLong() {
		return "REAL";
	}
	
	@Override
	public String getTypeFloat() {
		return "REAL";
	}
	
	@Override
	public String getTypeDouble() {
		return "REAL";
	}
	
	@Override
	public String getTypeCharacter() {
		return "TEXT";
	}
	
	@Override
	public String getTypeString() {
		return "TEXT";
	}
	
	@Override
	public String getTypeDate() {
		return "TEXT";
	}
	
	@Override
	public String executeCommandQueryCreateSQL() {
		if(this.constraints.length == 0) {
			return this.name + " " + this.type;
		} else {
			return this.name + " " + this.type + " " + String.join(" ", this.constraints);
		}
	}
	
	@Override
	public String executeCommandQueryDeleteSQL() {
		return "";
	}
	
	@Override
	public String executeCommandQueryCreateSQLTableAssociation() {
		
		StringBuilder sb = new StringBuilder();
		int i = 1;
		
		sb.append("CREATE TABLE IF NOT EXISTS ");
		sb.append(this.tableAssociation.getName());
		sb.append("(\n");
		
		//Champ clé primaire premiere table

		for(ColumnORM column : this.tableSource.getColumnsPrimaryKeys().values()) {
			sb.append(this.tableSource.getName() + "_" + this.name + "_" + column.getName() + "_" + i + " " + column.getType());
			sb.append(",\n");
			i++;
		}
		
		//Champ clé primaire seconde table
		
		for(ColumnORM column : this.tableTarget.getColumnsPrimaryKeys().values()) {
			sb.append(this.tableTarget.getName() + "_" + this.name + "_" + column.getName() + "_" + i + " " + column.getType());
			sb.append(",\n");
			i++;
		}	
		
		//Tous les champs forment une clé primaire
		
		i=1;
		sb.append("PRIMARY KEY(");
		
		for(ColumnORM column : this.tableSource.getColumnsPrimaryKeys().values()) {
			sb.append(this.tableSource.getName() + "_" + this.name + "_" + column.getName() + "_" + i);
			sb.append(",");
			i++;
		}
		
		for(ColumnORM column : this.tableTarget.getColumnsPrimaryKeys().values()) {
			sb.append(this.tableTarget.getName() + "_" + this.name + "_" + column.getName() + "_" + i);
			sb.append(",");
			i++;
		}
		
		//On supprime la dernière virgule
		sb.deleteCharAt(sb.length() - 1);
		sb.append("),\n");
		
		//Première table source
		
		i=1;
		sb.append("FOREIGN KEY(");
		
		for(ColumnORM column : this.tableSource.getColumnsPrimaryKeys().values()) {
			sb.append(this.tableSource.getName() + "_" + this.name + "_" + column.getName() + "_" + i);
			sb.append(",");
			i++;
		}
		
		//On supprime la dernière virgule
		sb.deleteCharAt(sb.length() - 1);
		
		sb.append(") REFERENCES ");
		sb.append(this.tableSource.getName());
		sb.append("(");
		
		for(ColumnORM column : this.tableSource.getColumnsPrimaryKeys().values()) {
			sb.append(column.getName());
			sb.append(",");
		}
		
		//On supprime la dernière virgule
		sb.deleteCharAt(sb.length() - 1);
		
		sb.append(") ");	
		
		sb.append(this.tableAssociation.getConstraintOnDelete());
		sb.append(" ");
		sb.append(this.tableAssociation.getConstraintOnUpdate());
		
		//Seconde table cible
		
		if(!this.primitiveValue) {
			sb.append(",\n");	
			sb.append("FOREIGN KEY(");
			
			for(ColumnORM column : this.tableTarget.getColumnsPrimaryKeys().values()) {
				sb.append(this.tableTarget.getName() + "_" + this.name + "_" + column.getName() + "_" + i);
				sb.append(",");
				i++;
			}
			
			//On supprime la dernière virgule
			sb.deleteCharAt(sb.length() - 1);
			
			sb.append(") REFERENCES ");
			sb.append(this.tableTarget.getName());
			sb.append("(");
			
			for(ColumnORM column : this.tableTarget.getColumnsPrimaryKeys().values()) {
				sb.append(column.getName());
				sb.append(",");
			}
			
			//On supprime la dernière virgule
			sb.deleteCharAt(sb.length() - 1);
			sb.append(") ");
			
			sb.append(this.tableAssociation.getConstraintOnDelete());
			sb.append(" ");
			sb.append(this.tableAssociation.getConstraintOnUpdate());	
		}
		
		sb.append("\n);");
		
		return sb.toString();
	}

	@Override
	public String executeCommandQueryDeleteSQLTableAssociation() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("DROP TABLE IF EXISTS ");
		sb.append(this.tableAssociation.getName());
		sb.append(";");
		
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return "ColumnForeignKeySQLite [tableSource=" + tableSource + ", tableTarget=" + tableTarget
				+ ", tableAssociation=" + tableAssociation + ", onCreateCascade=" + onCreateCascade + ", onReadCascade="
				+ onReadCascade + ", onUpdateCascade=" + onUpdateCascade + ", onDeleteCascade=" + onDeleteCascade
				+ ", field=" + field + ", clazz=" + clazz + ", primitiveValue=" + primitiveValue + ", defaultValue="
				+ defaultValue + ", collection=" + collection + ", array=" + array + ", name=" + name + ", type=" + type
				+ ", constraints=" + Arrays.toString(constraints) + "]";
	}
}
