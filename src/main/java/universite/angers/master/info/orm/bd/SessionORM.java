package universite.angers.master.info.orm.bd;

import java.util.Collection;
import java.util.Map;
import org.apache.log4j.Logger;

import universite.angers.master.info.orm.bd.column.ColumnORM;
import universite.angers.master.info.orm.bd.table.TableORM;

/**
 * Classe qui permet de se connecter à une base de données
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class SessionORM {

	/**
	 * Log de classe SessionORM
	 */
	private static final Logger LOG = Logger.getLogger(SessionORM.class);
	
	/**
	 * Créer ou non la BD
	 */
	protected boolean create;
	
	/**
	 * Nom de la BD
	 */
	protected String name;
	
	/**
	 * Chemin où est enregistrée la BD
	 */
	protected String path;
	
	/**
	 * Fournisseur BD
	 */
    protected String provider;
    
    /**
     * Host BD
     */
    protected String host;
    
    /**
     * Port BD
     */
    protected String port;
	
    /**
     * Identifiant de connexion
     */
    protected String userName;
    
    /**
     * Mot de passe de connexion
     */
    protected String password;
    
    /**
     * La base de donnée rattaché à la session
     */
    protected DataBaseORM dataBase;

	public SessionORM(boolean create, String name, String path, String provider, String host, String port,
			String userName, String password) {
		this.create = create;
		LOG.debug("Create : " + this.create);
		
		this.name = name;
		LOG.debug("Name : " + this.name);
		
		this.path = path;
		LOG.debug("Path : " + this.path);
		
		this.provider = provider;
		LOG.debug("Provider : " + this.provider);
		
		this.host = host;
		LOG.debug("Host : " + this.host);
		
		this.port = port;
		LOG.debug("Port : " + this.port);
		
		this.userName = userName;
		LOG.debug("UserName : " + this.userName);
		
		this.password = password;
		LOG.debug("Password : " + this.password);
	}

	public abstract boolean create(TableORM table);
	public abstract boolean delete(TableORM table);
	public abstract boolean create();
	public abstract boolean delete();
	public abstract boolean open();
	public abstract boolean close();
	
	public abstract boolean executeQuery(String commandSQL);
	public abstract boolean executeCreate(TableORM table, Object object);
	public abstract Collection<Object> executeRead(TableORM table, Map<String, ? extends ColumnORM> columnsSelect, 
			Map<? extends ColumnORM, Object> columnsWhere);
	public abstract boolean executeUpdate(TableORM table, Object object);
	public abstract boolean executeDelete(TableORM table, Object object);
	
	/**
	 * @return the dataBase
	 */
	public DataBaseORM getDataBase() {
		return dataBase;
	}

	/**
	 * @param dataBase the dataBase to set
	 */
	public void setDataBase(DataBaseORM dataBase) {
		this.dataBase = dataBase;
	}

	/**
	 * @return the create
	 */
	public boolean isCreate() {
		return create;
	}

	/**
	 * @param create the create to set
	 */
	public void setCreate(boolean create) {
		this.create = create;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (create ? 1231 : 1237);
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result + ((provider == null) ? 0 : provider.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionORM other = (SessionORM) obj;
		if (create != other.create)
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		if (provider == null) {
			if (other.provider != null)
				return false;
		} else if (!provider.equals(other.provider))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SessionORM [create=" + create + ", name=" + name + ", path=" + path + ", provider=" + provider
				+ ", host=" + host + ", port=" + port + ", userName=" + userName + ", password=" + password + "]";
	}
}
