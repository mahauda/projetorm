package universite.angers.master.info.orm.bd;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Classe qui permet d'énumérer les types particulier de Java afin d'effectuer des traitements supplémentaires
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class WrapperType {

	/**
	 * Log de classe ColumnORM
	 */
	private static final Logger LOG = Logger.getLogger(WrapperType.class);
	
	private static final Map<Class<?>, Class<?>> WRAPPERS = initWrappers();
	private static final String PATTERN_DATE = "MM/dd/yyyy HH:mm:ss:SSS Z";
	
	private static final Map<Class<?>, Class<?>> initWrappers() {
		Map<Class<?>, Class<?>> wrappers = new HashMap<>();
		
		wrappers.put(boolean.class, Boolean.class);
		wrappers.put(byte.class, Byte.class);
		wrappers.put(short.class, Short.class);
		wrappers.put(int.class, Integer.class);
		wrappers.put(long.class, Long.class);
		wrappers.put(float.class, Float.class);
		wrappers.put(double.class, Double.class);
		wrappers.put(char.class, Character.class);
		wrappers.put(void.class, Void.class);
		wrappers.put(String.class, String.class);
		wrappers.put(Date.class, Date.class);
		wrappers.put(Enum.class, Enum.class);
		
		return wrappers;
	}
	
	private WrapperType() {
		
	}
	
	public static boolean isBoolean(Class<?> type) {
		return boolean.class.isAssignableFrom(type) || Boolean.class.isAssignableFrom(type);
	}
	
	public static boolean isByte(Class<?> type) {
		return byte.class.isAssignableFrom(type) || Byte.class.isAssignableFrom(type);
	}
	
	public static boolean isShort(Class<?> type) {
		return short.class.isAssignableFrom(type) || Short.class.isAssignableFrom(type);
	}
	
	public static boolean isInteger(Class<?> type) {
		return int.class.isAssignableFrom(type) || Integer.class.isAssignableFrom(type);
	}
	
	public static boolean isLong(Class<?> type) {
		return long.class.isAssignableFrom(type) || Long.class.isAssignableFrom(type);
	}
	
	public static boolean isFloat(Class<?> type) {
		return float.class.isAssignableFrom(type) || Float.class.isAssignableFrom(type);
	}
	
	public static boolean isDouble(Class<?> type) {
		return double.class.isAssignableFrom(type) || Double.class.isAssignableFrom(type);
	}
	
	public static boolean isChar(Class<?> type) {
		return char.class.isAssignableFrom(type) || Character.class.isAssignableFrom(type);
	}
	
	public static boolean isVoid(Class<?> type) {
		return void.class.isAssignableFrom(type) || Void.class.isAssignableFrom(type);
	}
	
	public static boolean isString(Class<?> type) {
		return String.class.isAssignableFrom(type);
	}
	
	public static boolean isDate(Class<?> type) {
		return Date.class.isAssignableFrom(type);
	}
	
	public static boolean isEnum(Class<?> type) {
		return Enum.class.isAssignableFrom(type);
	}
	
	public static boolean isNumber(Class<?> type) {
		return isByte(type) || isShort(type) || isInteger(type) || isLong(type) || isFloat(type) || isDouble(type);
	}
	
	public static boolean isPrimitive(Class<?> type) {
		if(!WRAPPERS.containsKey(type)) {
			if(!WRAPPERS.containsValue(type)) {
				if(type.getSuperclass() != null) {
					return isPrimitive(type.getSuperclass());
				} else {
					return false;
				}
			} else return true;
		} else return true;
	}
	
	/**
	 * Les arrays primitives et wrapper ne sont pas convertit automatiquement par Java
	 * Seul les valeurs primitives et wrapper atomiques sont convertit par le mécanisme d'autoboxing et unboxing 
	 * En effet un array est un objet qui ne peut pas être caster facilement
	 * Par exemple boolean[] est un objet et Boolean[] est un autre objet a part
	 * Caster un array primitive vers un wrapper ou inversement provoque un CastException
	 * @param wrapper
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object convertWrapperPrimitive(Class<?> type, Object value) {
		if(type == null) return null;
		if(value == null) return null;
		
		if(type.isAssignableFrom(boolean[].class) && value.getClass().isAssignableFrom(Boolean[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Boolean[].class) && value.getClass().isAssignableFrom(boolean[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		else if(type.isAssignableFrom(byte[].class) && value.getClass().isAssignableFrom(Byte[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Byte[].class) && value.getClass().isAssignableFrom(byte[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		else if(type.isAssignableFrom(short[].class) && value.getClass().isAssignableFrom(Short[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Short[].class) && value.getClass().isAssignableFrom(short[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		else if(type.isAssignableFrom(int[].class) && value.getClass().isAssignableFrom(Integer[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Integer[].class) && value.getClass().isAssignableFrom(int[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		else if(type.isAssignableFrom(long[].class) && value.getClass().isAssignableFrom(Long[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Long[].class) && value.getClass().isAssignableFrom(long[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		else if(type.isAssignableFrom(float[].class) && value.getClass().isAssignableFrom(Float[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Float[].class) && value.getClass().isAssignableFrom(float[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		else if(type.isAssignableFrom(double[].class) && value.getClass().isAssignableFrom(Double[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Double[].class) && value.getClass().isAssignableFrom(double[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		else if(type.isAssignableFrom(char[].class) && value.getClass().isAssignableFrom(Character[].class)) {
			return convertArrayWrapperToArrayPrimitive(value);
		}
		else if(type.isAssignableFrom(Character[].class) && value.getClass().isAssignableFrom(char[].class)) {
			return convertArrayPrimitiveToArrayWrapper(value);
		}
		//Cas des booléens qui sont rengistrés par des chiffres
		//0 = Faux
		//1 = Vrai
		else if(isBoolean(type) && isInteger(value.getClass())) {
			if((int)value == 1) {
				return true;
			} else {
				return false;
			}
		} 
		else if(isBoolean(type) && isBoolean(value.getClass())) {
			if((boolean)value) {
				return 1;
			} else {
				return 0;
			}
		}
		//Cas des char qui sont enregistrés en string
		//On prend le premier caractère 
		//Sinon valeur par défaut
		else if(isChar(type) && isString(value.getClass())) {
			String s = (String)value;
			LOG.debug("Char s : " + s);
			if(s.length() > 0) {
				return s.charAt(0);
			} else {
				return Character.MIN_VALUE;
			}
		}
		else if(isChar(type) && isChar(value.getClass())) {
			return ((Character)value).toString();
		}
		else if(isDate(type) && isString(value.getClass())) {
			try {
				return new SimpleDateFormat(PATTERN_DATE).parse((String)value);
			} catch (ParseException e) {
				return null;
			}  
		}
		else if(isDate(type) && isDate(value.getClass())) {
			return new SimpleDateFormat(PATTERN_DATE).format((Date)value);
		}
		else if(isEnum(type) && isString(value.getClass())) {
			return Enum.valueOf((Class<Enum>)type, (String)value);
		} 
		else if(isEnum(type) && isEnum(value.getClass())) {
			return ((Enum)value).name();
		}
		else return value;
	}
	
	/**
	 * Les arrays primitives et wrapper ne sont pas convertit automatiquement par Java
	 * Seul les valeurs primitives et wrapper atomiques sont convertit par le mécanisme d'autoboxing et unboxing 
	 * En effet un array est un objet qui ne peut pas être caster facilement
	 * Par exemple boolean[] est un objet et Boolean[] est un autre objet a part
	 * Caster un array primitive vers un wrapper ou inversement provoque un CastException
	 * @param wrapper
	 * @return
	 */
	public static Object convertArrayPrimitiveToArrayWrapper(Object primitive) {
		if(primitive == null) return primitive;
		
		if(primitive.getClass().isAssignableFrom(boolean[].class)) {
			boolean[] arrayPrimitive = (boolean[]) primitive;
			Boolean[] arrayWrapper = new Boolean[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else if(primitive.getClass().isAssignableFrom(byte[].class)) {
			byte[] arrayPrimitive = (byte[]) primitive;
			Byte[] arrayWrapper = new Byte[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else if(primitive.getClass().isAssignableFrom(short[].class)) {
			short[] arrayPrimitive = (short[]) primitive;
			Short[] arrayWrapper = new Short[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else if(primitive.getClass().isAssignableFrom(int[].class)) {
			int[] arrayPrimitive = (int[]) primitive;
			Integer[] arrayWrapper = new Integer[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else if(primitive.getClass().isAssignableFrom(long[].class)) {
			long[] arrayPrimitive = (long[]) primitive;
			Long[] arrayWrapper = new Long[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else if(primitive.getClass().isAssignableFrom(float[].class)) {
			float[] arrayPrimitive = (float[]) primitive;
			Float[] arrayWrapper = new Float[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else if(primitive.getClass().isAssignableFrom(double[].class)) {
			double[] arrayPrimitive = (double[]) primitive;
			Double[] arrayWrapper = new Double[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else if(primitive.getClass().isAssignableFrom(char[].class)) {
			char[] arrayPrimitive = (char[]) primitive;
			Character[] arrayWrapper = new Character[arrayPrimitive.length];
			
			for(int i=0; i<arrayPrimitive.length; i++) {
				arrayWrapper[i] = arrayPrimitive[i];
			}
			
			return arrayWrapper;
		}
		else return primitive;
	}
	
	/**
	 * Les arrays primitives et wrapper ne sont pas convertit automatiquement par Java
	 * Seul les valeurs primitives et wrapper atomiques sont convertit par le mécanisme d'autoboxing et unboxing 
	 * En effet un array est un objet qui ne peut pas être caster facilement
	 * Par exemple boolean[] est un objet et Boolean[] est un autre objet a part
	 * Caster un array primitivier vers un wrapper ou inversement provoque un CastException
	 * @param wrapper
	 * @return
	 */
	public static Object convertArrayWrapperToArrayPrimitive(Object wrapper) {
		if(wrapper == null) return wrapper;
		
		if(wrapper.getClass().isAssignableFrom(Boolean[].class)) {
			Boolean[] arrayWrapper = (Boolean[]) wrapper;
			boolean[] arrayPrimitive = new boolean[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		else if(wrapper.getClass().isAssignableFrom(Byte[].class)) {
			Byte[] arrayWrapper = (Byte[]) wrapper;
			byte[] arrayPrimitive = new byte[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		else if(wrapper.getClass().isAssignableFrom(Short[].class)) {
			Short[] arrayWrapper = (Short[]) wrapper;
			short[] arrayPrimitive = new short[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		else if(wrapper.getClass().isAssignableFrom(Integer[].class)) {
			Integer[] arrayWrapper = (Integer[]) wrapper;
			int[] arrayPrimitive = new int[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		else if(wrapper.getClass().isAssignableFrom(Long[].class)) {
			Long[] arrayWrapper = (Long[]) wrapper;
			long[] arrayPrimitive = new long[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		else if(wrapper.getClass().isAssignableFrom(Float[].class)) {
			Float[] arrayWrapper = (Float[]) wrapper;
			float[] arrayPrimitive = new float[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		else if(wrapper.getClass().isAssignableFrom(Double[].class)) {
			Double[] arrayWrapper = (Double[]) wrapper;
			double[] arrayPrimitive = new double[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		else if(wrapper.getClass().isAssignableFrom(Character[].class)) {
			Character[] arrayWrapper = (Character[]) wrapper;
			char[] arrayPrimitive = new char[arrayWrapper.length];
			
			for(int i=0; i<arrayWrapper.length; i++) {
				arrayPrimitive[i] = arrayWrapper[i];
			}
			
			return arrayPrimitive;
		}
		//Les autres variables seront automatiquement convertit par le mécanisme d'autoboxing et unboxing 
		else return wrapper;
	}
}
