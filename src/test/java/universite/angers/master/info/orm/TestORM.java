package universite.angers.master.info.orm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.bd.jdbc.sqlite.SessionSQLite;
import universite.angers.master.info.orm.dao.EmployeDAO;
import universite.angers.master.info.orm.model.Address;
import universite.angers.master.info.orm.model.Boss;
import universite.angers.master.info.orm.model.Company;
import universite.angers.master.info.orm.model.Employe;
import universite.angers.master.info.orm.model.Job;
import universite.angers.master.info.orm.model.Man;
import universite.angers.master.info.orm.model.Person;
import universite.angers.master.info.orm.model.Skill;
import universite.angers.master.info.orm.model.Technician;
import universite.angers.master.info.orm.model.Woman;

/**
 * Test de l'ORM
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TestORM {

	private static final boolean CREATE = true;
	private static final String NAME = "sample.db";
	private static final String PATH = "/media/etudiant/2DCF15A2510DBD36/Cours";
	private static final String PROVIDER = "jdbc:sqlite";
	private static final String HOST = "";
	private static final String PORT = "";
	
	private static final Logger LOG = Logger.getLogger(TestORM.class);
	
	public static void main(String[] args) {
		LOG.debug("Connexion avec la bd");
	       
        String realPath = PATH;
        LOG.debug("Real path : " + realPath);
        
        //On crée la session avec JDBC
        SessionSQLite session = new SessionSQLite(CREATE, NAME, realPath, PROVIDER, HOST, PORT);
		LOG.debug("Session : " + session);
		
		//Construction classe par classe
		FactorySQLite.getInstance()
				.addSession(session)
				.addClassObject(Address.class)
				.addClassObject(Skill.class)
				.addClassObject(Boss.class)
				.addClassObject(Technician.class)
				.addClassObject(Employe.class)
				.addClassObject(Person.class)
				.addClassObject(Company.class)
				.addClassObject(Man.class)
				.addClassObject(Woman.class)
				.build();

		//Construction en fournisant le package qui contient tous les models
//		FactorySQLite.getInstance()
//			.addSession(session)
//			.addClassObject("iut.angers.master.info.orm.model")
//			.build();
		
		boolean open = FactorySQLite.getInstance().getDataBase().open();
		LOG.debug("Open : " + open);
		
		boolean delete = FactorySQLite.getInstance().getDataBase().delete();
		LOG.debug("Delete : " + delete);
		
		boolean create = FactorySQLite.getInstance().getDataBase().create();
		LOG.debug("Create : " + create);
		
		//Skills
		
		Skill computer = new Skill(1, "Computer", 10, Arrays.asList(1.0, 1.1, 1.2), new Double[] {1.3, 1.4, 1.5}, new double[] {1.6, 1.7, 1.8});
		Skill communication = new Skill(2, "Communication", 5, Arrays.asList(2.0, 2.1, 2.2), new Double[] {2.3, 2.4, 2.5}, new double[] {2.6, 2.7, 2.8});
		
		Set<Skill> highLevels = new HashSet<>();
		highLevels.add(computer);
		highLevels.add(communication);
		
		Skill[] lowLevels = new Skill[] {computer, communication};
		
		//Address
		
		Address addrTotoPrimary = new Address(1, "AdressTotoPrimary", "Place", 44200, "Nantes", Arrays.asList(1, 2, 3), new Integer[] {4, 5, 6}, new int[] {7, 8, 9});
		Address addrMomoPrimary = new Address(6, "AdressMomoPrimary", "Rue", 49000, "Angers", Arrays.asList(1, 2, 3), new Integer[] {4, 5, 6}, new int[] {7, 8, 9});
		Address addrNanaPrimary = new Address(3, "AdressNanaPrimary", "Place", 44200, "Angers", Arrays.asList(1, 2, 3), new Integer[] {4, 5, 6}, new int[] {7, 8, 9});
		
		Address addrTotoSecondary = new Address(4, "AdressTotoSecondary", "Place", 44200, "Nantes", Arrays.asList(10, 20, 30), new Integer[] {40, 50, 60}, new int[] {70, 80, 90});
		Address addrMomoSecondary = new Address(5, "AdressMomoSecondary", "Rue", 49000, "Angers", Arrays.asList(10, 20, 30), new Integer[] {40, 50, 60}, new int[] {70, 80, 90});
		Address addrNanaSecondary = new Address(6, "AdressNanaSecondary", "Place", 44200, "Angers", Arrays.asList(10, 20, 30), new Integer[] {40, 50, 60}, new int[] {70, 80, 90});
		
		Set<Address> addressPrimarySet = new TreeSet<>();
		addressPrimarySet.add(addrTotoPrimary);
		addressPrimarySet.add(addrMomoPrimary);
		addressPrimarySet.add(addrNanaPrimary);
		
		List<Address> addressPrimaryList = new LinkedList<>();
		addressPrimaryList.add(addrTotoPrimary);
		addressPrimaryList.add(addrMomoPrimary);
		addressPrimaryList.add(addrNanaPrimary);
		
		Address[] addressSecondary = new Address[] {addrTotoSecondary, addrMomoSecondary, addrNanaSecondary};
		
		List<String> publicMessages = Arrays.asList("Lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit.", "Vivamus", "rhoncus.");
		String[] privateMessages = new String[] {"il m'aime", "un peu", "beaucoup", "passionnément", "à la folie", "plus que tout", "pas du tout"};
		
		//Personnes
		
		Employe mohamed1 = new Employe("Mohamed1", "OUHIRRA1", "Momo1", null, null, null, addressPrimaryList, addressSecondary, addrMomoPrimary, 1000, 'M', publicMessages, privateMessages, new Date(), true, Job.DEVELOPER);
		Employe mohamed2 = new Employe("Mohamed2", "OUHIRRA2", "Momo2", null, null, null, addressPrimaryList, addressSecondary, null, 2000, 'M', publicMessages, privateMessages, new Date(), false, Job.DEVELOPER);
		
		Technician anas1 = new Technician("Anas1", "TAGUENITI1", "Nana1", null, null, null, addressPrimarySet, addressSecondary, addrNanaPrimary, 3000, 'M', publicMessages, privateMessages, new Date(), true, Job.DEVELOPER);
		Technician anas2 = new Technician("Anas2", "TAGUENITI2", "Nana2", null, null, null, addressPrimarySet, addressSecondary, addrNanaPrimary, 4000, 'M', publicMessages, privateMessages, new Date(), false, Job.DEVELOPER);		
		Technician anas3 = new Technician("Anas3", "TAGUENITI3", "Nana3", null, null, null, addressPrimarySet, addressSecondary, addrNanaPrimary, 5000, 'M', publicMessages, privateMessages, new Date(), true, Job.DEVELOPER);		
		
		Boss theo1 = new Boss("Theo1", "MAHAUDA1", "Toto1", null, null, null, highLevels, lowLevels, computer, 6000, 'M', publicMessages, privateMessages, new Date(), Job.PROJECT_MANAGER);
		Boss theo2 = new Boss("Theo2", "MAHAUDA2", "Toto2", null, null, null, highLevels, lowLevels, computer, 7000, 'M', publicMessages, privateMessages, new Date(), Job.PROJECT_MANAGER);
		Boss theo3 = new Boss("Theo3", "MAHAUDA3", "Toto3", null, null, null, highLevels, lowLevels, computer, 8000, 'M', publicMessages, privateMessages, new Date(), Job.PROJECT_MANAGER);
		Boss theo4 = new Boss("Theo4", "MAHAUDA4", "Toto4", null, null, null, highLevels, lowLevels, computer, 9000, 'M', publicMessages, privateMessages, new Date(), Job.PROJECT_MANAGER);
		
		//Les amis de momo 1
		//Il ne peut pas etre amis avec lui meme
		//Probleme de bidirectionnalité non géré
		
		List<Person> goodFriendsMomo = new ArrayList<>();
		goodFriendsMomo.add(anas1);
		goodFriendsMomo.add(anas2);
		goodFriendsMomo.add(theo1);
		goodFriendsMomo.add(theo2);
		goodFriendsMomo.add(theo3);
		
		Person[] badFriendsMomo = new Person[] {anas3, theo4};
		
		mohamed1.setGoodFriends(goodFriendsMomo);
		mohamed1.setBadFriends(badFriendsMomo);
		mohamed1.setBestFriend(anas1);
		
		mohamed2.setGoodFriends(goodFriendsMomo);
		mohamed2.setBadFriends(badFriendsMomo);
		mohamed2.setBestFriend(theo1);
		
//		/**
//		 * ADDRESSES
//		 */
//		
//		boolean createAddrTotoPrimary = AddressDAO.getInstance().create(addrTotoPrimary); //OK
//		LOG.debug("CreateAddrTotoPrimary : " + createAddrTotoPrimary);
//		
//		boolean createAddrMomoPrimary = AddressDAO.getInstance().create(addrMomoPrimary); //OK
//		LOG.debug("CreateAddrMomoPrimary : " + createAddrMomoPrimary);
//		
//		boolean createAddrNanaPrimary = AddressDAO.getInstance().create(addrNanaPrimary); //OK
//		LOG.debug("CreateAddrNanaPrimary : " + createAddrNanaPrimary);
//		
//		//Conclusion create address : OK
//		
//		Collection<Address> address = AddressDAO.getInstance().read(); //OK
//		LOG.debug("Address : " + address);
//		for(Address addr : address) {
//			LOG.debug("Address create : " + addr);
//		}
//		
//		//Conclusion read address : OK
//		
//		Map<String, Object> where = new HashMap<>();
//		where.put("codePostal", "49000");
//		
//		address = AddressDAO.getInstance().read(where);
//		for(Address addr : address) {
//			LOG.debug("Address create where : " + addr);
//		}
//		
//		//Conclusion read address with condition : OK
//		
//		addrTotoPrimary.setCodePostal(123);
//		addrTotoPrimary.setNumerosArrayPrimitive(new int[] {1111});
//		addrTotoPrimary.setNumerosArrayWrapper(new Integer[] {2222});
//		addrTotoPrimary.setNumerosList(null);
//		boolean updateAddrTotoPrimary = AddressDAO.getInstance().update(addrTotoPrimary); //OK
//		LOG.debug("UpdateAddrTotoPrimary : " + updateAddrTotoPrimary);
//		
//		addrMomoPrimary.setCodePostal(456);
//		addrMomoPrimary.setNumerosArrayPrimitive(new int[] {3333});
//		addrMomoPrimary.setNumerosArrayWrapper(null);
//		addrMomoPrimary.setNumerosList(Arrays.asList(4444));
//		boolean updateAddrMomoPrimary = AddressDAO.getInstance().update(addrMomoPrimary); //OK
//		LOG.debug("UpdateAddrMomoPrimary : " + updateAddrMomoPrimary);
//		
//		addrNanaPrimary.setCodePostal(789);
//		addrNanaPrimary.setNumerosArrayPrimitive(null);
//		addrNanaPrimary.setNumerosArrayWrapper(new Integer[] {5555});
//		addrNanaPrimary.setNumerosList(Arrays.asList(6666));
//		boolean updateAddrNanaPrimary = AddressDAO.getInstance().update(addrNanaPrimary); //OK
//		LOG.debug("UpdateAddrNanaPrimary : " + updateAddrNanaPrimary);
//		
//		//Conclusion update address : OK
//		
//		address = AddressDAO.getInstance().read(); //OK
//		LOG.debug("Address : " + address);
//		for(Address addr : address) {
//			LOG.debug("Address update : " + addr);
//		}
//		
//		//Conclusion read address : OK
//		
//		boolean deleteAddrTotoPrimary = AddressDAO.getInstance().delete(addrTotoPrimary); //OK
//		LOG.debug("DeleteAddrTotoPrimary : " + deleteAddrTotoPrimary);
//		
//		boolean deleteAddrMomoPrimary = AddressDAO.getInstance().delete(addrMomoPrimary); //OK
//		LOG.debug("DeleteAddrMomoPrimary : " + deleteAddrMomoPrimary);
//		
//		boolean deleteAddrNanaPrimary = AddressDAO.getInstance().delete(addrNanaPrimary); //OK
//		LOG.debug("DeleteAddrNanaPrimary : " + deleteAddrNanaPrimary);
//		
//		//Conclusion delete address : OK
//		
//		address = AddressDAO.getInstance().read(); //OK
//		LOG.debug("Address : " + address);
//		for(Address addr : address) {
//			LOG.debug("Address delete : " + addr);
//		}
//		
//		//Conclusion read address : OK
//		
//		/**
//		 * EMPLOYES
//		 */
//		
//		boolean createMomo1 = EmployeDAO.getInstance().create(mohamed1);
//		LOG.debug("CreateMomo1 : " + createMomo1);
//		
//		boolean createMomo2 = EmployeDAO.getInstance().create(mohamed2);
//		LOG.debug("CreateMomo2 : " + createMomo2);
//		
//		//Conclusion create employe : OK
//		
//		Collection<Employe> employes = EmployeDAO.getInstance().readAll(Collections.singletonMap("bestFriend", theo1)); //OK
//		LOG.debug("Employes : " + employes);
//		for(Employe emp : employes) {
//			LOG.debug("Employe create : " + emp);
//		}
//		
//		//Conclusion read employe : OK
//		
//		addrMomoPrimary.setCodePostal(456);
//		addrMomoPrimary.setNumerosArrayPrimitive(new int[] {3333});
//		addrMomoPrimary.setNumerosArrayWrapper(null);
//		addrMomoPrimary.setNumerosList(Arrays.asList(4444));
//		mohamed1.setNickName("Momomomo1");
//		boolean updateMomo1 = EmployeDAO.getInstance().update(mohamed1); //OK
//		LOG.debug("UpdateMomo1 : " + updateMomo1);
//		
//		addrNanaPrimary.setCodePostal(789);
//		addrNanaPrimary.setNumerosArrayPrimitive(null);
//		addrNanaPrimary.setNumerosArrayWrapper(new Integer[] {5555});
//		addrNanaPrimary.setNumerosList(Arrays.asList(6666));
//		mohamed2.setNickName("Momomomo2");
//		boolean updateMomo2 = EmployeDAO.getInstance().update(mohamed2); //OK
//		LOG.debug("UpdateMomo2 : " + updateMomo2);
//		
//		//Conclusion update address : OK
//		
//		employes = EmployeDAO.getInstance().read(); //OK
//		LOG.debug("Employes : " + employes);
//		for(Employe emp : employes) {
//			LOG.debug("Employe update : " + emp);
//		}
//		
//		EmployeDAO.getInstance().delete(mohamed1);
//		EmployeDAO.getInstance().delete(mohamed2);
	}
}
