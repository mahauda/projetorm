package universite.angers.master.info.orm.model;

import java.util.Arrays;
import java.util.Queue;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table
public class Company {

	@PrimaryKey
	private int id;
	
	@Column
	private String libelle;
	
	/**
	 * Utilisation de PriorityQueue pour tous types de personnes
	 */
	@ForeignKey
	private Queue<Person> personns;
	
	/**
	 * Uniquement les boss
	 */
	@ForeignKey
	private Boss[] boss;
	
	/**
	 * Uniquement employés
	 */
	@ForeignKey
	private Employe[] employees;
	
	/**
	 * Uniquement techniciens
	 */
	@ForeignKey
	private Technician[] technicians;
	
	public Company() {
		this(0, null, null, null, null, null);
	}
	
	public Company(int id, String libelle, Queue<Person> personns, Boss[] boss, Employe[] employees,
			Technician[] technicians) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.personns = personns;
		this.boss = boss;
		this.employees = employees;
		this.technicians = technicians;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * @return the personns
	 */
	public Queue<Person> getPersonns() {
		return personns;
	}

	/**
	 * @param personns the personns to set
	 */
	public void setPersonns(Queue<Person> personns) {
		this.personns = personns;
	}

	/**
	 * @return the boss
	 */
	public Boss[] getBoss() {
		return boss;
	}

	/**
	 * @param boss the boss to set
	 */
	public void setBoss(Boss[] boss) {
		this.boss = boss;
	}

	/**
	 * @return the employees
	 */
	public Employe[] getEmployees() {
		return employees;
	}

	/**
	 * @param employees the employees to set
	 */
	public void setEmployees(Employe[] employees) {
		this.employees = employees;
	}

	/**
	 * @return the technicians
	 */
	public Technician[] getTechnicians() {
		return technicians;
	}

	/**
	 * @param technicians the technicians to set
	 */
	public void setTechnicians(Technician[] technicians) {
		this.technicians = technicians;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(boss);
		result = prime * result + Arrays.hashCode(employees);
		result = prime * result + id;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + ((personns == null) ? 0 : personns.hashCode());
		result = prime * result + Arrays.hashCode(technicians);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (!Arrays.equals(boss, other.boss))
			return false;
		if (!Arrays.equals(employees, other.employees))
			return false;
		if (id != other.id)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (personns == null) {
			if (other.personns != null)
				return false;
		} else if (!personns.equals(other.personns))
			return false;
		if (!Arrays.equals(technicians, other.technicians))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", libelle=" + libelle + ", personns=" + personns + ", boss="
				+ Arrays.toString(boss) + ", employees=" + Arrays.toString(employees) + ", technicians="
				+ Arrays.toString(technicians) + "]";
	}
}
