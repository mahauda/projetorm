package universite.angers.master.info.orm.dao;

import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.orm.model.Address;

/**
 * DAO address
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AddressDAO extends DAO<Address> {

	private static AddressDAO addressDAO;
	
	private AddressDAO() {
		super(FactorySQLite.getInstance(), Address.class);
	}
	
	public static AddressDAO getInstance() {
		if(addressDAO == null)
			addressDAO = new AddressDAO();
		
		return addressDAO;
	}
}
