package universite.angers.master.info.orm.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table
public class Employe extends Woman {

	@Column
	private double salaire;
	
	@Column
	private Boolean newArrival;
	
	/**
	 * Utilisation LinkedList
	 */
	@ForeignKey
	private List<Address> addressPrimary;
	
	@ForeignKey
	private Address[] addressSecondary;
	
	@ForeignKey
	private Address addressMain;

	public Employe() {
		this(null, null, null, new ArrayList<>(), new Person[0], null, new LinkedList<>(), new Address[0], null, 0, 'F', 
				new ArrayList<>(), new String[0], new Date(), false, Job.TECHNICIAN);
	}

	public Employe(String firstName, String lastName, String nickName, List<Person> goodFriends, Person[] badFriends,
			Person bestFriend, List<Address> addressPrimary, Address[] addressSecondary, Address addressMain,
			double salaire, char sexe, List<String> publicMessages, String[] privateMessages, Date birth, Boolean newArrival, Job job) {
		super(firstName, lastName, nickName, goodFriends, badFriends, bestFriend, sexe, publicMessages, privateMessages, birth, job);
		this.addressPrimary = addressPrimary;
		this.addressSecondary = addressSecondary;
		this.addressMain = addressMain;
		this.salaire = salaire;
		this.newArrival = newArrival;
	}
	
	/**
	 * @return the newArrival
	 */
	public Boolean getNewArrival() {
		return newArrival;
	}

	/**
	 * @param newArrival the newArrival to set
	 */
	public void setNewArrival(Boolean newArrival) {
		this.newArrival = newArrival;
	}

	/**
	 * @return the addressPrimary
	 */
	public List<Address> getAddressPrimary() {
		return addressPrimary;
	}

	/**
	 * @param addressPrimary the addressPrimary to set
	 */
	public void setAddressPrimary(List<Address> addressPrimary) {
		this.addressPrimary = addressPrimary;
	}

	/**
	 * @return the addressSecondary
	 */
	public Address[] getAddressSecondary() {
		return addressSecondary;
	}

	/**
	 * @param addressSecondary the addressSecondary to set
	 */
	public void setAddressSecondary(Address[] addressSecondary) {
		this.addressSecondary = addressSecondary;
	}

	/**
	 * @return the addressMain
	 */
	public Address getAddressMain() {
		return addressMain;
	}

	/**
	 * @param addressMain the addressMain to set
	 */
	public void setAddressMain(Address addressMain) {
		this.addressMain = addressMain;
	}

	/**
	 * @return the salaire
	 */
	public double getSalaire() {
		return salaire;
	}

	/**
	 * @param salaire the salaire to set
	 */
	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((addressMain == null) ? 0 : addressMain.hashCode());
		result = prime * result + ((addressPrimary == null) ? 0 : addressPrimary.hashCode());
		result = prime * result + Arrays.hashCode(addressSecondary);
		long temp;
		temp = Double.doubleToLongBits(salaire);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employe other = (Employe) obj;
		if (addressMain == null) {
			if (other.addressMain != null)
				return false;
		} else if (!addressMain.equals(other.addressMain))
			return false;
		if (addressPrimary == null) {
			if (other.addressPrimary != null)
				return false;
		} else if (!addressPrimary.equals(other.addressPrimary))
			return false;
		if (!Arrays.equals(addressSecondary, other.addressSecondary))
			return false;
		if (Double.doubleToLongBits(salaire) != Double.doubleToLongBits(other.salaire))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employe [salaire=" + salaire + ", addressPrimary=" + addressPrimary + ", addressSecondary="
				+ Arrays.toString(addressSecondary) + ", addressMain=" + addressMain + ", sexe=" + sexe + ", firstName="
				+ firstName + ", lastName=" + lastName + ", nickName=" + nickName + ", publicMessages=" + publicMessages
				+ ", privateMessages=" + Arrays.toString(privateMessages) + "]";
	}
}
