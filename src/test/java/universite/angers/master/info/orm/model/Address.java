package universite.angers.master.info.orm.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table(isAutoIncrement = true)
public class Address implements Comparable<Address> {

	@PrimaryKey
	private int id;
	
	@Column
	private String libelle;
	
	@Column
	private String place;
	
	@Column
	private int codePostal;
	
	@Column
	private String ville;
	
	@ForeignKey
	private List<Integer> numerosList;
	
	@ForeignKey
	private Integer[] numerosArrayWrapper;
	
	@ForeignKey
	private int[] numerosArrayPrimitive;

	/**
	 * Constructeur par défaut sans argument pour l'ORM
	 */
	public Address() {
		this(0, "", "", 0, "", new ArrayList<>(), new Integer[0], new int[0]);
	}
	
	public Address(int id, String libelle, String place, int codePostal, String ville, 
			List<Integer> numerosList, Integer[] numerosArrayWrapper, int[] numerosArrayPrimitive) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.place = place;
		this.codePostal = codePostal;
		this.ville = ville;
		this.numerosList = numerosList;
		this.numerosArrayWrapper = numerosArrayWrapper;
		this.numerosArrayPrimitive = numerosArrayPrimitive;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}

	/**
	 * @return the codePostal
	 */
	public int getCodePostal() {
		return codePostal;
	}

	/**
	 * @param codePostal the codePostal to set
	 */
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * @return the numerosList
	 */
	public List<Integer> getNumerosList() {
		return numerosList;
	}

	/**
	 * @param numerosList the numerosList to set
	 */
	public void setNumerosList(List<Integer> numerosList) {
		this.numerosList = numerosList;
	}

	/**
	 * @return the numerosArrayWrapper
	 */
	public Integer[] getNumerosArrayWrapper() {
		return numerosArrayWrapper;
	}

	/**
	 * @param numerosArrayWrapper the numerosArrayWrapper to set
	 */
	public void setNumerosArrayWrapper(Integer[] numerosArrayWrapper) {
		this.numerosArrayWrapper = numerosArrayWrapper;
	}

	/**
	 * @return the numerosArrayPrimitive
	 */
	public int[] getNumerosArrayPrimitive() {
		return numerosArrayPrimitive;
	}

	/**
	 * @param numerosArrayPrimitive the numerosArrayPrimitive to set
	 */
	public void setNumerosArrayPrimitive(int[] numerosArrayPrimitive) {
		this.numerosArrayPrimitive = numerosArrayPrimitive;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codePostal;
		result = prime * result + id;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + Arrays.hashCode(numerosArrayPrimitive);
		result = prime * result + Arrays.hashCode(numerosArrayWrapper);
		result = prime * result + ((numerosList == null) ? 0 : numerosList.hashCode());
		result = prime * result + ((place == null) ? 0 : place.hashCode());
		result = prime * result + ((ville == null) ? 0 : ville.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (codePostal != other.codePostal)
			return false;
		if (id != other.id)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (!Arrays.equals(numerosArrayPrimitive, other.numerosArrayPrimitive))
			return false;
		if (!Arrays.equals(numerosArrayWrapper, other.numerosArrayWrapper))
			return false;
		if (numerosList == null) {
			if (other.numerosList != null)
				return false;
		} else if (!numerosList.equals(other.numerosList))
			return false;
		if (place == null) {
			if (other.place != null)
				return false;
		} else if (!place.equals(other.place))
			return false;
		if (ville == null) {
			if (other.ville != null)
				return false;
		} else if (!ville.equals(other.ville))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Address [id=" + id + ", libelle=" + libelle + ", place=" + place + ", codePostal=" + codePostal
				+ ", ville=" + ville + ", numerosList=" + numerosList + ", numerosArrayWrapper="
				+ Arrays.toString(numerosArrayWrapper) + ", numerosArrayPrimitive="
				+ Arrays.toString(numerosArrayPrimitive) + "]";
	}

	@Override
	public int compareTo(Address o) {
		return 0;
	}
}
