package universite.angers.master.info.orm.model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table(isAutoIncrement = true)
public class Skill {

	@PrimaryKey
	private int id;
	
	@Column
	private String libelle;
	
	@Column
	private int level;

	@ForeignKey
	private List<Double> numerosList;
	
	@ForeignKey
	private Double[] numerosArrayWrapper;
	
	@ForeignKey
	private double[] numerosArrayPrimitive;
	
	public Skill() {
		this(0, "", 0, new LinkedList<>(), new Double[0], new double[0]);
	}
	
	public Skill(int id, String libelle, int level, List<Double> numerosList, 
			Double[] numerosArrayWrapper, double[] numerosArrayPrimitive) {
		this.id = id;
		this.libelle = libelle;
		this.level = level;
		this.numerosList = numerosList;
		this.numerosArrayWrapper = numerosArrayWrapper;
		this.numerosArrayPrimitive = numerosArrayPrimitive;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}
	
	/**
	 * @return the numerosList
	 */
	public List<Double> getNumerosList() {
		return numerosList;
	}

	/**
	 * @param numerosList the numerosList to set
	 */
	public void setNumerosList(List<Double> numerosList) {
		this.numerosList = numerosList;
	}

	/**
	 * @return the numerosArrayWrapper
	 */
	public Double[] getNumerosArrayWrapper() {
		return numerosArrayWrapper;
	}

	/**
	 * @param numerosArrayWrapper the numerosArrayWrapper to set
	 */
	public void setNumerosArrayWrapper(Double[] numerosArrayWrapper) {
		this.numerosArrayWrapper = numerosArrayWrapper;
	}

	/**
	 * @return the numerosArrayPrimitive
	 */
	public double[] getNumerosArrayPrimitive() {
		return numerosArrayPrimitive;
	}

	/**
	 * @param numerosArrayPrimitive the numerosArrayPrimitive to set
	 */
	public void setNumerosArrayPrimitive(double[] numerosArrayPrimitive) {
		this.numerosArrayPrimitive = numerosArrayPrimitive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + level;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + Arrays.hashCode(numerosArrayPrimitive);
		result = prime * result + Arrays.hashCode(numerosArrayWrapper);
		result = prime * result + ((numerosList == null) ? 0 : numerosList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Skill other = (Skill) obj;
		if (id != other.id)
			return false;
		if (level != other.level)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (!Arrays.equals(numerosArrayPrimitive, other.numerosArrayPrimitive))
			return false;
		if (!Arrays.equals(numerosArrayWrapper, other.numerosArrayWrapper))
			return false;
		if (numerosList == null) {
			if (other.numerosList != null)
				return false;
		} else if (!numerosList.equals(other.numerosList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Skill [id=" + id + ", libelle=" + libelle + ", level=" + level + ", numerosList=" + numerosList
				+ ", numerosArrayWrapper=" + Arrays.toString(numerosArrayWrapper) + ", numerosArrayPrimitive="
				+ Arrays.toString(numerosArrayPrimitive) + "]";
	}
}
