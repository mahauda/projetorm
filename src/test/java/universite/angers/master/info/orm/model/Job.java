package universite.angers.master.info.orm.model;

public enum Job {

	TECHNICIAN("TECHNICIAN"),
	DEVELOPER("DEVELOPER"),
	
	MANAGER("MANAGER"),
	PROJECT_MANAGER("PROJECT_MANAGER");
	
	private String name;
	
	private Job(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
