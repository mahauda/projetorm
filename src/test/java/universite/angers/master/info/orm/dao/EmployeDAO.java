package universite.angers.master.info.orm.dao;

import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.orm.model.Employe;

/**
 * DAO address
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class EmployeDAO extends DAO<Employe> {

	private static EmployeDAO employeDAO;
	
	private EmployeDAO() {
		super(FactorySQLite.getInstance(), Employe.class);
	}
	
	public static EmployeDAO getInstance() {
		if(employeDAO == null)
			employeDAO = new EmployeDAO();
		
		return employeDAO;
	}
}
